package com.cvut.shoppingshare.activity;

import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.EntityListMessage;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.drawer.AbstractNavDrawerActivity;
import com.cvut.shoppingshare.drawer.NavDrawerActivityConfiguration;
import com.cvut.shoppingshare.drawer.NavDrawerAdapter;
import com.cvut.shoppingshare.drawer.NavDrawerItem;
import com.cvut.shoppingshare.drawer.NavMenuItem;
import com.cvut.shoppingshare.drawer.NavMenuSection;
import com.cvut.shoppingshare.fragment.AddGroupFragment;
import com.cvut.shoppingshare.fragment.AddTemplateFragment;
import com.cvut.shoppingshare.fragment.GroupMembersFragment;
import com.cvut.shoppingshare.fragment.GroupPurchasesFragment;
import com.cvut.shoppingshare.fragment.MembershipOffersFragment;
import com.cvut.shoppingshare.fragment.SearchGroupFragment;
import com.cvut.shoppingshare.fragment.TemplatesFragment;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.task.LogoutTask;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Activity with new NavigationDrawer.
 *
 * Created by marek on 1/23/14.
 */
public class ShoppingShareMainActivity extends AbstractNavDrawerActivity implements AddGroupFragment.AddGroupFragmentCallbacks,
        GroupPurchasesFragment.GroupPurchasesFragmentCallbacks, GroupMembersFragment.GroupMembersFragmentCallbacks,
        TemplatesFragment.TemplatesFragmentCallbacks, AddTemplateFragment.AddTemplateFragmentCallbacks {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if ( savedInstanceState == null ) {
//            getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();
        }
        loadOrDownloadGroups();
    }

    @Override
    protected NavDrawerActivityConfiguration getNavDrawerConfiguration() {
        List<NavDrawerItem> menuList = new ArrayList<NavDrawerItem>();
        menuList.add(NavMenuSection.create(new Long(-1), "Groups"));
        menuList.add(NavMenuSection.create(new Long(-3), "Templates"));
        menuList.add(NavMenuItem.create(new Long(-24), "Templates", android.R.drawable.ic_menu_edit, true));
        menuList.add(NavMenuItem.create(new Long(-26), "Create", android.R.drawable.ic_menu_add, true));
        menuList.add(NavMenuSection.create(new Long(-2), "General"));
        menuList.add(NavMenuItem.create(new Long(-21), "Create group", android.R.drawable.ic_menu_add, true));
        menuList.add(NavMenuItem.create(new Long(-22), "Search group", android.R.drawable.ic_menu_search, true));
        menuList.add(NavMenuItem.create(new Long(-23), "Offers", android.R.drawable.ic_menu_manage, true));
        menuList.add(NavMenuItem.create(new Long(-25), "Logout", android.R.drawable.ic_menu_directions, false));

        NavDrawerActivityConfiguration navDrawerActivityConfiguration = new NavDrawerActivityConfiguration();
        navDrawerActivityConfiguration.setMainLayout(R.layout.main);
        navDrawerActivityConfiguration.setDrawerLayoutId(R.id.drawer_layout);
        navDrawerActivityConfiguration.setLeftDrawerId(R.id.left_drawer);
        navDrawerActivityConfiguration.setNavItems(menuList);
        navDrawerActivityConfiguration.setDrawerShadow(R.drawable.drawer_shadow);
        navDrawerActivityConfiguration.setDrawerOpenDesc(R.string.drawer_open);
        navDrawerActivityConfiguration.setDrawerCloseDesc(R.string.drawer_close);
        drawerAdapter = new NavDrawerAdapter(this, R.layout.navdrawer_item, menuList);
        navDrawerActivityConfiguration.setBaseAdapter(drawerAdapter);
        return navDrawerActivityConfiguration;
    }

    @Override
    protected void onGroupSelected(Long groupId) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, GroupPurchasesFragment.newInstance(groupId))
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onSearchGroupSelected() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, SearchGroupFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    protected void onShowMembershipOffersSelected() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, MembershipOffersFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showGroupMembersFragment(Long groupId) {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, GroupMembersFragment.newInstance(groupId))
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onShowTemplatesSelected() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, TemplatesFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showAddTemplateFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, AddTemplateFragment.newInstance())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void showProgressBar() {
        mPullToRefreshLayout.setRefreshing(true);
    }

    @Override
    public void dismissProgressBar() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    @Override
    public void addCreatedGroup(EntityListMessage message) {
        new PersistCreatedGroupTask().execute(message);
    }

    private void handleAddCreatedGroup(EntityListMessage message) {
        NavMenuItem item = NavMenuItem.create(message.getId(), message.getName(), android.R.drawable.ic_menu_agenda, true);
        int position = drawerAdapter.addItemToGroupSection(item);
        selectItem(position);
        Log.i("SS", "Persisting created group succeeded. ");
    }

    @Override
    protected void onAddGroupSelected() {
        Log.i("SS", "Going to create new group.");
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.container, AddGroupFragment.newInstance())
                .commit();
    }

    @Override
    protected void onLogoutSelected() {
        new LogoutTask(this).execute();
    }

    private void loadOrDownloadGroups() {
        showProgressBar();
        new LoadAllGroupsFromDatabaseTask().execute();
    }

    private void handleLoadedGroups(List<NavDrawerItem> navDrawerItems) {
        for (NavDrawerItem item: navDrawerItems) {
            drawerAdapter.addItemToGroupSection(item);
        }
        Log.i("SS", "Loading groups from database finished.");
        downloadGroupNews();
    }

    @Override
    protected void downloadGroupNews() {
        new DownloadGroupNewsTask().execute();
    }

    private void handleDownloadedGroupNews(List<NavDrawerItem> navDrawerItems) {
        if (navDrawerItems == null) {
            dismissProgressBar();
            return;
        }
        for (NavDrawerItem item: navDrawerItems) {
            drawerAdapter.addItemToGroupSection(item);
        }
        Log.i("SS", "Downloading groups' news finished.");
        dismissProgressBar();
    }

    private class LoadAllGroupsFromDatabaseTask extends AsyncTask<Void, Void, List<NavDrawerItem>> {

        @Override
        protected List<NavDrawerItem> doInBackground(Void... voids) {
            Log.i("SS", "Loading groups from database.");
            List<NavDrawerItem> groups = new ArrayList<NavDrawerItem>();

            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();
            Cursor cursor = ShoppingShareDatabaseManager.getAllGroups(db);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long groupId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.SharegroupEntry._ID)
                    );
                    String groupName = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.SharegroupEntry.COLUMN_NAME_NAME)
                    );
                    groups.add(NavMenuItem.create(groupId, groupName, android.R.drawable.ic_menu_agenda, true));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
            return groups;
        }

        @Override
        protected void onPostExecute(List<NavDrawerItem> navDrawerItems) {
            handleLoadedGroups(navDrawerItems);
        }
    }

    private class DownloadGroupNewsTask extends AsyncTask<Void, Void, List<NavDrawerItem>> {

        private Date newSync;

        @Override
        protected List<NavDrawerItem> doInBackground(Void... voids) {
            Log.i("SS", "Downloading groups' news.");
            Date lastSync = getLastSynchronizationDate();
            Log.i("SS", "Groups' last sync was in: " + lastSync);

            EntityListMessage[] result = downloadGroupNews(lastSync);
            if (result != null) {
                setLastSynchronizationDate(newSync);
                updateDatabaseWithGroupNews(result);
                return groupListMessageToNavDrawerItem(result);
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<NavDrawerItem> navDrawerItems) {
            handleDownloadedGroupNews(navDrawerItems);
        }

        private Date getLastSynchronizationDate() {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();
            Cursor cursor = ShoppingShareDatabaseManager.getLastGroupSynchronizationDate(db);
            cursor.moveToFirst();
            if (cursor.isAfterLast()) {
                Log.i("SS", "Groups have never been synchronized.");
                ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
                return null;
            } else {
                try {
                    Long time = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE)
                    );
                    return new Date(time);
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                    return null;
                } finally {
                    ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
                }
            }
         }

        private EntityListMessage[] downloadGroupNews(Date lastSync) {
            if (lastSync == null) {
                lastSync = new Date(1);
            }
            try {
                final String url = getString(R.string.base_uri) + "/group/all";

                SharedPreferences sharedPref = getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                // Populate the headers in an HttpEntity object to use for the request
                HttpEntity<Date> requestEntity = new HttpEntity<Date>(lastSync, requestHeaders);

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                // Perform the HTTP GET request
                ResponseEntity<EntityListMessage[]> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        EntityListMessage[].class);

                newSync = new Date(responseEntity.getHeaders().getDate());

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Dowloading groups' news failed." + e.getMessage(), e);
                return null;
            }
        }

        private void setLastSynchronizationDate(Date newSync) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.setLastGroupSynchronizationDate(db, newSync);
            ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithGroupNews(EntityListMessage[] news) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();
            boolean updated;
            for (int i = 0; i < news.length; i++) {
                updated = ShoppingShareDatabaseManager.updateOrInsertGroup(db, news[i]);
                if (updated) {
                    drawerAdapter.removeItemWithId(news[i].getId());
                }
            }
            ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
        }

        private List<NavDrawerItem> groupListMessageToNavDrawerItem(EntityListMessage[] news) {
            ArrayList<NavDrawerItem> items = new ArrayList<NavDrawerItem>();
            for (EntityListMessage glm: news) {
                items.add(NavMenuItem.create(glm.getId(), glm.getName(), android.R.drawable.ic_menu_agenda, true));
            }
            return items;
        }
    }

    private class PersistCreatedGroupTask extends AsyncTask<EntityListMessage, Void, Boolean> {

        private EntityListMessage message;

        @Override
        protected Boolean doInBackground(EntityListMessage... messages) {
            message = messages[0];
            try {
                SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();
                ShoppingShareDatabaseManager.updateOrInsertGroup(db, message);
                ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
                return true;
            } catch (Exception e) {
                Log.e("SS", "Persisting created group failed. " + e + e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            dismissProgressBar();
            if (success) {
                handleAddCreatedGroup(message);
            }
        }
    }
}
