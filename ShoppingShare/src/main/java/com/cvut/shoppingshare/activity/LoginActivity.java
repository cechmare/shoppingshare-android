package com.cvut.shoppingshare.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.database.ShoppingShareDbHelper;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.NetworkUtil;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpAuthentication;
import org.springframework.http.HttpBasicAuthentication;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;
import java.util.List;

public class LoginActivity extends Activity {

    private static final String TRY_TO_CONNECT = "TRY_TO_CONNECT";

    private static final String DB_OWNER = "DB_OWNER";

    private LinearLayout loginProgressLayout;

    private EditText email;

    private EditText password;

    private String emailString;

    private String passwordString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        loginProgressLayout = (LinearLayout) findViewById(R.id.LoginProgressLayout);
        email = (EditText) findViewById(R.id.login_email);
        password = (EditText) findViewById(R.id.login_password);

        Intent intent = getIntent();
        boolean tryToConnect = intent.getBooleanExtra(TRY_TO_CONNECT, true);
        if (tryToConnect) {
            tryToLoginAutomatically();
        }
    }

    public void signUp(View view) {
        Intent intent = new Intent(this, SignUpActivity.class);
        startActivity(intent);
    }

    public void login(View view) {
        boolean online = NetworkUtil.getConnectivityStatus(getApplicationContext());
        if (!online) {
            Toast.makeText(getApplicationContext(), getString(R.string.connection_lost), Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressBar();
        emailString = email.getText().toString();
        passwordString = password.getText().toString();
        new LoginTask().execute();
    }

    private void tryToLoginAutomatically() {
        boolean online = NetworkUtil.getConnectivityStatus(getApplicationContext());
        if (!online) {
            Toast.makeText(getApplicationContext(), getString(R.string.connection_lost), Toast.LENGTH_SHORT).show();
            return;
        }
        showProgressBar();
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        emailString = sharedPref.getString(getString(R.string.preferences_username), "def");
        passwordString = sharedPref.getString(getString(R.string.preferences_password), "def");
        if (!emailString.equals("def")) {
            //TODO
            Toast.makeText(this, "MAM username: " + emailString + " a pass: " + password, Toast.LENGTH_SHORT).show();
            email.setText(emailString);
            this.password.setText(passwordString);
            new LoginTask().execute();
        } else {
            dismissProgressBar();
        }
    }

    private void showProgressBar() {
        loginProgressLayout.setVisibility(View.VISIBLE);
    }

    private void dismissProgressBar() {
        loginProgressLayout.setVisibility(View.INVISIBLE);
    }

    private void handleResult(ConfirmMessage confirmMessage) {
        if (confirmMessage != null && confirmMessage.isOk()) {
            boolean changeDbOwner = checkDatabaseOwner();
            SharedPreferences sharedPref = getSharedPreferences(
                    getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.preferences_username), emailString);
            editor.putString(getString(R.string.preferences_password), passwordString);
            if (changeDbOwner) {
                editor.putString(DB_OWNER, emailString);
            }
            editor.commit();
            if (!changeDbOwner) {
                dismissProgressBar();
                startMainActivity();
            }
        } else {
            dismissProgressBar();
            Toast.makeText(this, getString(R.string.login_fail), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkDatabaseOwner() {
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        String currentOwner = sharedPref.getString(DB_OWNER, "def");
        if (!currentOwner.equals(emailString)) {
            Log.i("SS", "Current DB owner isn't the logged one -> going to clear DB.");
            new ClearDbTask().execute();
            return true;
        } else {
            Log.i("SS", "Current DB owner is the logged one -> no need to clear database.");
            return false;
        }
    }

    private void startMainActivity() {
        Toast.makeText(this, getString(R.string.login_success), Toast.LENGTH_SHORT).show();
        Log.i("SS", "Going to start MainActivity." );
        Intent intent = new Intent(this, ShoppingShareMainActivity.class);
        startActivity(intent);
    }

    private class ClearDbTask extends AsyncTask<Void, Void, Boolean> {

        @Override
        protected Boolean doInBackground(Void... voids) {
            try {
                SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getApplicationContext()).openDatabase();

                db.execSQL(ShoppingShareContract.PurchaseTemplateEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.MembershipOfferEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.UserGroupEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.UserEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.MembershipRequestEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.ItemEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.PurchaseEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.SharegroupEntry.SQL_DELETE);
                db.execSQL(ShoppingShareContract.LastSynchronization.SQL_DELETE);

                db.execSQL(ShoppingShareContract.SharegroupEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.PurchaseEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.ItemEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.UserEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.MembershipRequestEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.LastSynchronization.SQL_CREATE);
                db.execSQL(ShoppingShareContract.UserGroupEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.MembershipOfferEntry.SQL_CREATE);
                db.execSQL(ShoppingShareContract.PurchaseTemplateEntry.SQL_CREATE);

                ShoppingShareDatabaseManager.getInstance(getApplicationContext()).closeDatabase();
            } catch (SQLiteException e) {
                Log.e("SS", "Database couldn't be opened for writing, user can't log in." + e + e.getMessage());
                return false;
            }
            return true;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            dismissProgressBar();
            if (success) {
                startMainActivity();
            } else {
                //TODO handle situation when memory is full
            }
        }
    }

    private class LoginTask extends AsyncTask<Void, Void, ConfirmMessage> {

        @Override
        protected ConfirmMessage doInBackground(Void... params) {
            try {
                final String url = getString(R.string.base_uri) + "/login/basic";

                // Populate the HTTP Basic Authentitcation header with the username and password
                HttpAuthentication authHeader = new HttpBasicAuthentication(emailString, passwordString);
                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.setAuthorization(authHeader);
                requestHeaders.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<ConfirmMessage> response = restTemplate.exchange(url, HttpMethod.GET,
                        new HttpEntity<Object>(requestHeaders), ConfirmMessage.class);

                // Getting and storing JSESSIONID
                HttpHeaders responseHeaders = response.getHeaders();
                List<String> val = responseHeaders.get("Set-Cookie");
                if (val != null) {
                    saveSessionId(val.get(0));
                } else {
                    // TODO throw exception
                    Log.e("SS", "No JSESSIONID returned from server.");
                    throw new Exception("No JSESSIONID returned from server.");
                }

                return response.getBody();
            } catch (Exception e) {
                Log.e("SS", e.getMessage(), e);
                ConfirmMessage message = new ConfirmMessage();
                message.setOk(false);
                return message;
            }
        }

        @Override
        protected void onPostExecute(ConfirmMessage confirmMessage) {
            handleResult(confirmMessage);
        }

        private void saveSessionId(String sessionId) {
            Log.d("SS", "Storing sessionId: " + sessionId);
            SharedPreferences sharedPref = getSharedPreferences(
                    getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.preferences_session_id), sessionId);
            editor.commit();
        }
    }

}
