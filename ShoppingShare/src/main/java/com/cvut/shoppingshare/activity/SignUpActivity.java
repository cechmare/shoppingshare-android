package com.cvut.shoppingshare.activity;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.SignupForm;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.NetworkUtil;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class SignUpActivity extends Activity {

    private LinearLayout signupProgressLayout;

    private AutoCompleteTextView emailAutocomplete;

    private EditText password;

    private EditText passwordAgain;

    private String selectedEmail;

    private String selectedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        signupProgressLayout = (LinearLayout) findViewById(R.id.SignupProgressLayout);
        emailAutocomplete = (AutoCompleteTextView) findViewById(R.id.signup_autocomplete_email);
        String[] availableEmails = getAvailableEmails();
        ArrayAdapter<String> adapter =
                new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, availableEmails);
        emailAutocomplete.setAdapter(adapter);

        password = (EditText) findViewById(R.id.signup_password);
        passwordAgain = (EditText) findViewById(R.id.signup_password_again);
    }

    private String[] getAvailableEmails() {
        Pattern emailPattern = Patterns.EMAIL_ADDRESS;
        Account[] accounts = AccountManager.get(this).getAccounts();
        List<String> emails = new ArrayList<String>();
        for (Account account : accounts) {
            String possibleEmail = account.name;
            if (emailPattern.matcher(possibleEmail).matches()) {
                if (!emails.contains(possibleEmail)) {
                    emails.add(possibleEmail);
                }
            }
        }
        return emails.toArray(new String[emails.size()]);
    }

    public void signUp(View view) {
        boolean online = NetworkUtil.getConnectivityStatus(getApplicationContext());
        if (!online) {
            Toast.makeText(getApplicationContext(), getString(R.string.connection_lost), Toast.LENGTH_SHORT).show();
            return;
        }
        selectedEmail = (String) emailAutocomplete.getText().toString();
        if (selectedEmail.length() < 5) {
            Toast.makeText(this, getString(R.string.signup_select_email), Toast.LENGTH_SHORT).show();
            return;
        }

        selectedPassword = this.password.getText().toString();
        if (checkPassword(selectedPassword)) {
            return;
        }

        showProgressBar();
        SignupForm form = new SignupForm(selectedEmail, selectedPassword, null, null);
        new SignupTask().execute(form);
    }

    private boolean checkPassword(String selectedPassword) {
        String passwordAgain = this.passwordAgain.getText().toString();
        if (!selectedPassword.equals(passwordAgain)) {
            Toast.makeText(this, getString(R.string.signup_password_not_match), Toast.LENGTH_SHORT).show();
            return true;
        }
        if (selectedPassword.length() < 4) {
            Toast.makeText(this, getString(R.string.signup_password_too_short), Toast.LENGTH_SHORT).show();
            return true;
        }
        return false;
    }

    private void showProgressBar() {
        signupProgressLayout.setVisibility(View.VISIBLE);
    }

    private void dismissProgressBar() {
        signupProgressLayout.setVisibility(View.INVISIBLE);
    }

    private void handleResult(ConfirmMessage confirmMessage) {
        if (confirmMessage != null && confirmMessage.isOk()) {
            Toast.makeText(this, getString(R.string.signup_success), Toast.LENGTH_SHORT).show();
            saveUsernameAndPassword();
            //Intent intent = new Intent(this, LoginActivity.class);
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
        } else {
            Toast.makeText(this, getString(R.string.signup_fail), Toast.LENGTH_SHORT).show();
        }
    }

    private void saveUsernameAndPassword() {
        SharedPreferences sharedPref = getSharedPreferences(
                getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(getString(R.string.preferences_username), selectedEmail);
        editor.putString(getString(R.string.preferences_password), selectedPassword);
        editor.commit();
    }

    private class SignupTask extends AsyncTask<SignupForm, Void, ConfirmMessage> {

        @Override
        protected ConfirmMessage doInBackground(SignupForm... params) {
            try {
                final String url = getString(R.string.base_uri) + "/signup";

                HttpHeaders requestHeaders = new HttpHeaders();

                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<SignupForm> requestEntity = new HttpEntity<SignupForm>(params[0], requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<ConfirmMessage> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        ConfirmMessage.class);

                return response.getBody();
            } catch (Exception e) {
                Log.e("SS", e.getMessage(), e);
                ConfirmMessage message = new ConfirmMessage();
                message.setOk(false);
                return message;
            }
        }

        @Override
        protected void onPostExecute(ConfirmMessage confirmMessage) {
            dismissProgressBar();
            handleResult(confirmMessage);
        }


    }

}
