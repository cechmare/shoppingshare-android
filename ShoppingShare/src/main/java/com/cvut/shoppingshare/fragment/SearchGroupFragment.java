package com.cvut.shoppingshare.fragment;



import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.EntityListMessageListAdapter;
import com.cvut.shoppingshare.data.AutocompleteForm;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.EntityListMessage;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;


/**
 * Use the {@link SearchGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class SearchGroupFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final int AUTOCOMPLETE_DELAY = 2000;

    private static final int MESSAGE_TEXT_CHANGED = 0;

    private AutoCompleteTextView nameAutocomplete;

    private TextWatcher textWatcher;

    private Handler handler;

    private EntityListMessageListAdapter adapter;

    private EntityListMessage selectedGroup;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment SearchGroupFragment.
     */
    public static SearchGroupFragment newInstance() {
        SearchGroupFragment fragment = new SearchGroupFragment();
        return fragment;
    }

    public SearchGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        textWatcher = new SearchGroupTextWatcher();
        handler = new SearchGroupHandler();
        adapter = new EntityListMessageListAdapter(getActivity(), android.R.layout.simple_list_item_1);
        selectedGroup = null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search_group, container, false);

        nameAutocomplete = (AutoCompleteTextView) view.findViewById(R.id.search_group_autocomplete);
        nameAutocomplete.setAdapter(adapter);
        nameAutocomplete.addTextChangedListener(textWatcher);
        nameAutocomplete.setOnItemClickListener(this);

        Button createRequestButton = (Button) view.findViewById(R.id.button_create_request);
        createRequestButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendRequest(view);
            }
        });

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        selectedGroup = adapter.getEntity(position);
    }

    public void sendRequest(View view) {
        if (selectedGroup != null) {
            Log.d("SS", "Going to send request to group with id: " + selectedGroup.getId());

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setPositiveButton(R.string.create_request_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    new SendRequestTask().execute(selectedGroup.getId());
                }
            });
            builder.setMessage("Request " + selectedGroup.getName() + " for membership?");
            builder.create().show();
        } else {
            Toast.makeText(getActivity(), R.string.create_request_no_group_selected, Toast.LENGTH_SHORT).show();
        }
    }

    private class SearchGroupTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            handler.removeMessages(MESSAGE_TEXT_CHANGED);
            handler.sendEmptyMessageDelayed(MESSAGE_TEXT_CHANGED, AUTOCOMPLETE_DELAY);
        }

    }

    private class SearchGroupHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MESSAGE_TEXT_CHANGED) {
                String enteredText = nameAutocomplete.getText().toString();
                new SearchGroupTask().execute(new AutocompleteForm(enteredText));
            }
        }

    }

    private class SearchGroupTask extends AsyncTask<AutocompleteForm, Void, EntityListMessage[]> {

        @Override
        protected EntityListMessage[] doInBackground(AutocompleteForm... forms) {
            try {
                final String url = getString(R.string.base_uri) + "/group/startsWith";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<AutocompleteForm> requestEntity = new HttpEntity<AutocompleteForm>(forms[0], requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<EntityListMessage[]> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        EntityListMessage[].class);

                return response.getBody();
            } catch (Exception e) {
                Log.e("SS", e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(EntityListMessage[] groupListMessages) {
            if (groupListMessages != null) {
                adapter.changeDataset(Arrays.asList(groupListMessages));
            }
        }

    }

    private class SendRequestTask extends AsyncTask<Long, Void, ConfirmMessage> {

        @Override
        protected ConfirmMessage doInBackground(Long... longs) {
            try {
                final String url = getString(R.string.base_uri) + "/membershiprequest";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<Long> requestEntity = new HttpEntity<Long>(longs[0], requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> result = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, ConfirmMessage.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Sending membership request to group with id " + longs[0] + " resulted in: " + e + e.getMessage());
                return null;
            }
        }

        @Override
        protected void onPostExecute(ConfirmMessage message) {
            selectedGroup = null;
            if (message.isOk()) {
                Toast.makeText(getActivity(), R.string.create_request_done, Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), R.string.create_request_error, Toast.LENGTH_SHORT).show();
            }
        }

    }


}
