package com.cvut.shoppingshare.fragment;

import com.cvut.shoppingshare.data.ConfirmMessage;

/**
 * Created by marek on 2/28/14.
 */
public interface TemplateAddedListener {

    public void templateAdded(boolean success);

}
