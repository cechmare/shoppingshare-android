package com.cvut.shoppingshare.fragment;

import com.cvut.shoppingshare.data.ConfirmMessage;

/**
 * Created by marek on 17.3.14.
 */
public interface PurchaseOrTemplateEditedListener {

    public void handlePurchaseOrTemplateEdited(ConfirmMessage message);

}
