package com.cvut.shoppingshare.fragment;



import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.GroupPurchasesExpandableListAdapter;
import com.cvut.shoppingshare.dao.LastSynchronizationDao;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.Item;
import com.cvut.shoppingshare.data.ItemDto;
import com.cvut.shoppingshare.data.ItemState;
import com.cvut.shoppingshare.data.Purchase;
import com.cvut.shoppingshare.data.PurchaseDto;
import com.cvut.shoppingshare.data.PurchaseState;
import com.cvut.shoppingshare.data.PurchaseTemplateSourceEnum;
import com.cvut.shoppingshare.data.PurchasesItemsDto;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.dialog.AddItemDialog;
import com.cvut.shoppingshare.dialog.AddTemplateFromExisting;
import com.cvut.shoppingshare.dialog.DeleteItemDialog;
import com.cvut.shoppingshare.dialog.DeleteTemplateDialog;
import com.cvut.shoppingshare.dialog.EditItemDialog;
import com.cvut.shoppingshare.dialog.EditPurchaseOrTemplateDialog;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


public class TemplatesFragment extends Fragment implements OnRefreshListener, TemplateAddedListener,
        ItemDeletedListener, ItemAddedListener, ItemEditedListener, PurchaseOrTemplateEditedListener {

    private static final String ENTITY_ID = "ENTITY_ID";

    private static final String ITEM_ID = "ITEM_ID";

    private static final String TEMPLATE_ID = "TEMPLATE_ID";

    private static final String PURCHASE_ID = "PURCHASE_ID";

    private static final String TEMPLATE_SOURCE = "TEMPLATE_SOURCE";

    private static final String TEMPLATE = "TEMPLATE";

    private static final String ITEM = "ITEM";

    private static final String ENTITY = "ENTITY";

    private PullToRefreshLayout mPullToRefreshLayout;

    private ExpandableListView purchasesListView;

    private GroupPurchasesExpandableListAdapter adapter;

    private List<Purchase> purchases;

    private ActionMode templateActionMode;

    private TemplateActionModeCallback templateActionModeCallback;

    private ActionMode itemActionMode;

    private ItemActionModeCallback itemActionModeCallback;

    private TemplatesFragmentCallbacks mListener;

    public static TemplatesFragment newInstance() {
        TemplatesFragment fragment = new TemplatesFragment();
        return fragment;
    }

    public TemplatesFragment() {
        purchases = new ArrayList<Purchase>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_templates, container, false);
        mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.fragment_templates_pullable);
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create()
                        .scrollDistance(.15f)
                        .build())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        purchasesListView = (ExpandableListView) getActivity().findViewById(R.id.fragment_templates_expandable_list);
        adapter = new GroupPurchasesExpandableListAdapter(purchases, getActivity().getLayoutInflater());
        purchasesListView.setAdapter(adapter);
        purchasesListView.setOnItemLongClickListener(new TemplatesOnLongClickListener());

        templateActionMode = null;
        templateActionModeCallback = new TemplateActionModeCallback();
        itemActionMode = null;
        itemActionModeCallback = new ItemActionModeCallback();

        loadData(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fragment_templates_add_template:
                mListener.showAddTemplateFragment();
                return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_templates_menu, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (TemplatesFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddTemplateFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefreshStarted(View view) {
        downloadNews();
    }

    @Override
    public void templateAdded(boolean success) {
        if (success) {
            Toast.makeText(getActivity(), "Adding template succeeded!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Adding template failed!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handleItemDeleted(Long entityId) {
        adapter.deleteItemWithId(entityId);
        downloadNews();
    }

    @Override
    public void handleItemAdded(Item item, Long templateId) {
        if (item.getEntityId() == null) {
            Toast.makeText(getActivity(), "Adding item failed!", Toast.LENGTH_SHORT).show();
        } else {
            adapter.addItem(item, templateId);
            downloadNews();
        }
    }

    @Override
    public void handleItemEdited(ConfirmMessage message) {
        if (message.isOk()) {
            Toast.makeText(getActivity(), "Item successfully edited!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Item edit failed! Try to refresh.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handlePurchaseOrTemplateEdited(ConfirmMessage message) {
        if (message.isOk()) {
            Toast.makeText(getActivity(), "Template successfully edited!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Template edit failed! Try to refresh.", Toast.LENGTH_SHORT).show();
        }
    }

    public void handleTemplateDeleted(boolean success) {
        if (success) {
            Toast.makeText(getActivity(), "Deleting template succeeded!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Deleting template failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private void showAddTemplateFromTemplateDialog(Long templateId) {
        DialogFragment dialog = new AddTemplateFromExisting();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(ENTITY_ID, templateId);
        bundle.putSerializable(TEMPLATE_SOURCE, PurchaseTemplateSourceEnum.TEMPLATE);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "add_template");
    }

    private void showAddItemDialog(Long templateId) {
        DialogFragment dialog = new AddItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(PURCHASE_ID, templateId);
        bundle.putBoolean(TEMPLATE, true);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "add_item");
    }

    private void showDeleteItemDialog(Long itemId) {
        DialogFragment dialog = new DeleteItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(ITEM_ID, itemId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "delete_item");
    }

    private void showDeleteTemplateDialog(Long templateId) {
        DialogFragment dialog = new DeleteTemplateDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(TEMPLATE_ID, templateId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "delete_template");
    }

    private void showEditItemDialog(Long itemId) {
        Item item = adapter.getItemWithId(itemId);
        if (item == null) {
            return;
        }
        DialogFragment dialog = new EditItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM, item);
        bundle.putBoolean(TEMPLATE, true);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "edit_item");
    }

    private void showEditTemplateDialog(Long templateId) {
        Purchase purchase = adapter.getPurchaseWithId(templateId);
        if (purchase == null) {
            return;
        }
        DialogFragment dialog = new EditPurchaseOrTemplateDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putSerializable(ENTITY, purchase);
        bundle.putBoolean(TEMPLATE, true);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "edit_purchase");
    }

    private void showProgressBar() {
        mPullToRefreshLayout.setRefreshing(true);
    }

    private void dismissProgressBar() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    private void loadData(boolean downloadNews) {
        showProgressBar();
        new LoadDataFromDatabaseTask().execute(downloadNews);
    }

    private void handleLoadedData(List<Purchase> purchases, boolean downloadNews) {
        adapter.addPurchases(purchases);

        if (downloadNews) {
            downloadNews();
        } else {
            dismissProgressBar();
        }
    }

    private void downloadNews() {
        new DownloadNewsTask().execute();
    }

    private class LoadDataFromDatabaseTask extends AsyncTask<Boolean, Void, List<Purchase>> {

        private boolean downloadNews;

        @Override
        protected List<Purchase> doInBackground(Boolean... params) {
            downloadNews = params[0];

            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            Log.i("SS", "Loading purchases templates from database.");
            List<Purchase> purchases = getPurchasesTemplates(db);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return purchases;
        }

        @Override
        protected void onPostExecute(List<Purchase> templates) {
            handleLoadedData(templates, downloadNews);
        }

        private List<Purchase> getPurchasesTemplates(SQLiteDatabase db) {
            List<Purchase> templates = new ArrayList<Purchase>();
            Cursor cursor = ShoppingShareDatabaseManager.getPurchaseTemplates(db);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long templateId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseTemplateEntry._ID)
                    );
                    String name = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseTemplateEntry.COLUMN_NAME_NAME)
                    );
                    List<Item> items = getItemsOfPurchaseTemplates(db, templateId);
                    templates.add(new Purchase(templateId, items, name));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return  templates;
        }

        private List<Item> getItemsOfPurchaseTemplates(SQLiteDatabase db, Long templateId) {
            List<Item> items = new ArrayList<Item>();
            Cursor cursor = ShoppingShareDatabaseManager.getItemsOfPurchaseTemplate(db, templateId);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long itemId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.ItemEntry._ID)
                    );
                    String name = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.ItemEntry.COLUMN_NAME_NAME)
                    );
                    items.add(new Item(name, itemId));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return items;
        }

    }

    private class DownloadNewsTask extends AsyncTask<Void, Void, Boolean> {

        private Date newSync;

        @Override
        protected Boolean doInBackground(Void... voids) {
            Log.i("SS", "Downloading purchases templates news.");
            Date lastSync = LastSynchronizationDao.getLastPurchasesTemplatesDate(getActivity().getApplicationContext());
            Log.i("SS", "Last sync was in: " + lastSync);
            PurchasesItemsDto result = downloadNews(lastSync);
            if (result != null) {
                setLastSynchronizationDate(newSync);
                updateDatabaseWithTemplatesNews(result.getPurchases());
                updateDatabaseWithTemplateItemsNews(result.getItems());
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean updateGui) {
            if (updateGui) {
                loadData(false);
            }
        }

        private PurchasesItemsDto downloadNews(Date lastSync) {
            if (lastSync == null) {
                lastSync = new Date(1);
            }
            try {
                final String url = getString(R.string.base_uri) + "/purchasetemplate/news";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<Date> requestEntity = new HttpEntity<Date>(lastSync, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<PurchasesItemsDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        PurchasesItemsDto.class);

                newSync = new Date(responseEntity.getHeaders().getDate());

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Dowloading purchases templates news failed." + e.getMessage(), e);
                return null;
            }
        }

        private void setLastSynchronizationDate(Date newSync) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.setLastPurchasesTemplatesDate(db, newSync);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithTemplatesNews(List<PurchaseDto> purchases) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            for (PurchaseDto purchase: purchases) {
                if (purchase.getState() == PurchaseState.DELETED) {
                    ShoppingShareDatabaseManager.removePurchaseTemplateWithId(db, purchase.getPurchaseId());
                } else {
                    ShoppingShareDatabaseManager.updateOrInsertPurchaseTemplate(db, purchase);
                }
            }
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithTemplateItemsNews(List<ItemDto> items) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            for (ItemDto item: items) {
                if (item.getState() == ItemState.DELETED) {
                    ShoppingShareDatabaseManager.removeItemWithId(db, item.getItemId());
                } else {
                    ShoppingShareDatabaseManager.updateTemplateItemWithId(db, item);
                }
            }
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

    }

    private class TemplatesOnLongClickListener implements AdapterView.OnItemLongClickListener {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            Long entityId = (Long) view.getTag();
            if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                Log.i("SS", "Clicked item with id: " + entityId);
                if (itemActionMode != null) {
                    return false;
                }
                itemActionMode = getActivity().startActionMode(itemActionModeCallback);
                itemActionMode.setTag(entityId);
                view.setHovered(true);
                return true;
            } else if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                Log.i("SS", "Clicked template with id: " + entityId);
                if (templateActionMode != null) {
                    return false;
                }
                templateActionMode = getActivity().startActionMode(templateActionModeCallback);
                templateActionMode.setTag(entityId);
                view.setHovered(true);
                return true;
            } else {
                return false;
            }
        }
    }

    private class TemplateActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_templates_template_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long templateId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_templates_template:
                    Log.i("SS", "Going to create template from already existing one.");
                    showAddTemplateFromTemplateDialog(templateId);
                    return true;
                case R.id.menu_fragment_templates_delete_template:
                    Log.i("SS", "Going to delete template with id: " + templateId);
                    showDeleteTemplateDialog(templateId);
                    return true;
                case R.id.menu_fragment_templates_add_item:
                    Log.i("SS", "Going to add item to template with id: " + templateId);
                    showAddItemDialog(templateId);
                    return true;
                case R.id.menu_fragment_templates_edit_template:
                    Log.i("SS", "Going to edit template with id: " + templateId);
                    showEditTemplateDialog(templateId);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            purchasesListView.setItemChecked(-1, true);
            templateActionMode = null;
        }

    }

    private class ItemActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_templates_item_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long itemId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_templates_delete_item:
                    showDeleteItemDialog(itemId);
                    return true;
                case R.id.menu_fragment_templates_edit_item:
                    showEditItemDialog(itemId);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            purchasesListView.setItemChecked(-1, true);
            itemActionMode = null;
        }

    }

    public interface TemplatesFragmentCallbacks {

        public void showAddTemplateFragment();

    }

}
