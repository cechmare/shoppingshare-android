package com.cvut.shoppingshare.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.MembershipOffersListAdapter;
import com.cvut.shoppingshare.data.MembershipOfferDto;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.dialog.AcceptMembershipOfferDialog;
import com.cvut.shoppingshare.dialog.RejectMembershipOfferDialog;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

public class MembershipOffersFragment extends Fragment implements
        OnRefreshListener {

    private static final String OFFER_ID = "OFFER_ID";

    private ListView offersListView;

    private MembershipOffersListAdapter offersListAdapter;

    private ActionMode offersActionMode;

    private MembershipOffersCallback offersActionModeCallback;

    private PullToRefreshLayout mPullToRefreshLayout;

    // TODO: Rename and change types of parameters
    public static MembershipOffersFragment newInstance() {
        MembershipOffersFragment fragment = new MembershipOffersFragment();
        return fragment;
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MembershipOffersFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_membership_offers, container, false);

        offersListView = (ListView) view.findViewById(R.id.user_membership_offers_list_view);
        offersListAdapter = new MembershipOffersListAdapter(getActivity().getLayoutInflater());
        offersListView.setAdapter(offersListAdapter);
        offersListView.setOnItemLongClickListener(new MembershipOffersOnLongClickListener());

        mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.user_membership_offers_pullable);
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create()
                        .scrollDistance(.15f)
                        .build())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);

        loadMembershipOffers(true);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        offersActionMode = null;
        offersActionModeCallback = new MembershipOffersCallback();
    }

    @Override
    public void onRefreshStarted(View view) {
        downloadMembershipOffers();
    }

    public void removeDealtOffer(Long offerId) {
        new RemoveDealtMembershipOfferTask().execute(offerId);
    }

    private void showProgressBar() {
        mPullToRefreshLayout.setRefreshing(true);
    }

    private void dismissProgressBar() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    private void showAcceptMembershipOfferDialog(Long offerId) {
        DialogFragment dialog = new AcceptMembershipOfferDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(OFFER_ID, offerId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "confirm_offer");
    }

    private void showRejectMembershipOfferDialog(Long offerId) {
        DialogFragment dialog = new RejectMembershipOfferDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(OFFER_ID, offerId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "reject_offer");
    }

    private void loadMembershipOffers(boolean downloadNews) {
        showProgressBar();
        new LoadMembershipOffersFromDatabaseTask().execute(downloadNews);
    }

    private void handleLoadedMembershipOffers(List<MembershipOfferDto> offers, boolean downloadNews) {
        offersListAdapter.addItems(offers);

        if (downloadNews) {
            downloadMembershipOffers();
        } else {
            dismissProgressBar();
        }
    }

    public void downloadMembershipOffers() {
        new DownloadMembershipOffersNewsTask().execute();
    }

    private class LoadMembershipOffersFromDatabaseTask extends AsyncTask<Boolean, Void, List<MembershipOfferDto>> {

        private boolean downloadNews;

        @Override
        protected List<MembershipOfferDto> doInBackground(Boolean... params) {
            downloadNews = params[0];
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();

            Log.i("SS", "Loading membership offers from database.");
            List<MembershipOfferDto> offers = getMembershipOffers(db);

            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return offers;
        }

        @Override
        protected void onPostExecute(List<MembershipOfferDto> offers) {
            handleLoadedMembershipOffers(offers, downloadNews);
        }

        private List<MembershipOfferDto> getMembershipOffers(SQLiteDatabase db) {
            List<MembershipOfferDto> offers = new ArrayList<MembershipOfferDto>();
            Cursor cursor = ShoppingShareDatabaseManager.getAllMembershipOffers(db);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long requestId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipOfferEntry._ID)
                    );
                    String groupName = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipOfferEntry.COLUMN_SHAREGROUP_NAME)
                    );
                    offers.add(new MembershipOfferDto(requestId, groupName));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return offers;
        }
    }

    private class DownloadMembershipOffersNewsTask extends AsyncTask<Void, Void, Boolean> {

        private Date newSync;

        @Override
        protected Boolean doInBackground(Void... voids) {
            Log.i("SS", "Downloading user's membership offers.");
            Date lastSync = getLastSynchronizationDate();
            Log.i("SS", "Last sync was in: " + lastSync);

            MembershipOfferDto[] result = downloadMembershipOffersNews(lastSync);
            if (result != null) {
                setLastSynchronizationDate(newSync);
                updateDatabaseWithMembershipOffersNews(result);
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean updateGui) {
            if (updateGui) {
                offersListAdapter.removeAllItems();
                loadMembershipOffers(false);
            }
        }

        private Date getLastSynchronizationDate() {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            Cursor cursor = ShoppingShareDatabaseManager.getLastMembershipOffersDate(db);
            cursor.moveToFirst();
            if (cursor.isAfterLast()) {
                Log.i("SS", "Membership offers have never been synchronized.");
                ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
                return null;
            } else {
                try {
                    Long time = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE)
                    );
                    return new Date(time);
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                    return null;
                } finally {
                    ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
                }
            }
        }

        private MembershipOfferDto[] downloadMembershipOffersNews(Date lastSync) {
            if (lastSync == null) {
                lastSync = new Date(1);
            }
            try {
                final String url = getString(R.string.base_uri) + "/membershipoffer/all";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                // Populate the headers in an HttpEntity object to use for the request
                HttpEntity<Date> requestEntity = new HttpEntity<Date>(lastSync, requestHeaders);

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                // Perform the HTTP GET request
                ResponseEntity<MembershipOfferDto[]> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        MembershipOfferDto[].class);

                newSync = new Date(responseEntity.getHeaders().getDate());

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Dowloading membership offers' news failed." + e.getMessage(), e);
                return null;
            }
        }

        private void setLastSynchronizationDate(Date newSync) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.setLastMembershipOffersSynchronizationDate(db, newSync);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithMembershipOffersNews(MembershipOfferDto[] dtos) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            for (int i = 0; i < dtos.length; i++) {
                ShoppingShareDatabaseManager.insertMembershipOffer(db, dtos[i]);
            }
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }
    }

    private class RemoveDealtMembershipOfferTask extends AsyncTask<Long, Void, Void> {

        private Long offerId;

        @Override
        protected Void doInBackground(Long... longs) {
            offerId = longs[0];
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.removeMembershipOfferWithId(db, offerId);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            offersListAdapter.removeAllItems();
            loadMembershipOffers(false);
        }

    }

    private class MembershipOffersOnLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
            Long offerId = (Long) view.getTag();
            Log.i("SS", "Long-clicked membership offer with id: " + offerId);
            if (offersActionMode != null) {
                return false;
            }
            offersActionMode = getActivity().startActionMode(offersActionModeCallback);
            offersActionMode.setTag(offerId);
            view.setHovered(true);
            return true;
        }

    }

    private class MembershipOffersCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_membership_offers_item_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long offerId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_membership_offer_accept:
                    showAcceptMembershipOfferDialog(offerId);
                    offersActionMode.finish();
                    return true;
                case R.id.menu_fragment_membership_offer_reject:
                    showRejectMembershipOfferDialog(offerId);
                    offersActionMode.finish();
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            offersListView.setItemChecked(-1, true);
            offersActionMode = null;
        }

    }

}
