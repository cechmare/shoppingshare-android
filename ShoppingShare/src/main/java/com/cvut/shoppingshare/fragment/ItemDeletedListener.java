package com.cvut.shoppingshare.fragment;

/**
 * Created by marek on 3/5/14.
 */
public interface ItemDeletedListener {

    public void handleItemDeleted(Long itemId);

}
