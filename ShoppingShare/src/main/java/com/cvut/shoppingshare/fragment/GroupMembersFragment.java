package com.cvut.shoppingshare.fragment;

import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TabHost;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.GroupMembersListViewAdapter;
import com.cvut.shoppingshare.adapter.GroupMembershipOffersListAdapter;
import com.cvut.shoppingshare.adapter.GroupMembershipRequestsListAdapter;
import com.cvut.shoppingshare.data.DealtMembershipRequest;
import com.cvut.shoppingshare.data.GroupMembersMessage;
import com.cvut.shoppingshare.data.MembershipRequestDto;
import com.cvut.shoppingshare.data.MembershipRequestType;
import com.cvut.shoppingshare.data.NewsRequestMessage;
import com.cvut.shoppingshare.data.ShareUserDto;
import com.cvut.shoppingshare.data.TaskRequest;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.dialog.AcceptMembershipRequestDialog;
import com.cvut.shoppingshare.dialog.RejectMembershipRequestDialog;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link GroupMembersFragment.GroupMembersFragmentCallbacks} interface
 * to handle interaction events.
 * Use the {@link GroupMembersFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class GroupMembersFragment extends Fragment implements
        OnRefreshListener, TabHost.OnTabChangeListener {

    private static final String GROUP_ID = "GROUP_ID";

    private static final String REQUEST_ID = "REQUEST_ID";

    private static final String TAB_REQUESTS = "TAB_REQUESTS";

    private static final String TAB_MEMBERS = "TAB_MEMBERS";

    private static final String TAB_OFFERS = "TAB_OFFERS";

    private GroupMembersFragmentCallbacks mListener;

    private Long groupId;

    private TabHost tabHost;

    private ListView membersListView;

    private GroupMembersListViewAdapter membersAdapter;

    private ListView requestsListView;

    private GroupMembershipRequestsListAdapter requestsAdapter;

    private ActionMode requestsActionMode;

    private MembershipRequestsCallback requestsActionModeCallback;

    private ListView offersListView;

    private GroupMembershipOffersListAdapter offersAdapter;

    private PullToRefreshLayout mPullToRefreshLayout;
    private PullToRefreshLayout mPullToRefreshMembersLayout;
    private PullToRefreshLayout mPullToRefreshRequestsLayout;
    private PullToRefreshLayout mPullToRefreshOffersLayout;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GroupMembersFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GroupMembersFragment newInstance(Long groupId) {
        GroupMembersFragment fragment = new GroupMembersFragment();
        Bundle args = new Bundle();
        args.putLong(GROUP_ID, groupId);
        fragment.setArguments(args);
        return fragment;
    }
    public GroupMembersFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_group_members, container, false);

        mPullToRefreshMembersLayout = (PullToRefreshLayout) view.findViewById(R.id.fragemnt_group_members_members_tab_pullable);
        mPullToRefreshRequestsLayout = (PullToRefreshLayout) view.findViewById(R.id.fragemnt_group_members_requests_tab_pullable);
        mPullToRefreshOffersLayout = (PullToRefreshLayout) view.findViewById(R.id.fragemnt_group_members_offers_tab_pullable);

        tabHost = (TabHost) view.findViewById(R.id.group_members_tab_host);
        tabHost.setup();
        tabHost.setOnTabChangedListener(this);

        TabHost.TabSpec spec1 = tabHost.newTabSpec(TAB_MEMBERS);
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("Members");

        TabHost.TabSpec spec2 = tabHost.newTabSpec(TAB_REQUESTS);
        spec2.setIndicator("Requests");
        spec2.setContent(R.id.tab2);

        TabHost.TabSpec spec3 = tabHost.newTabSpec(TAB_OFFERS);
        spec3.setContent(R.id.tab3);
        spec3.setIndicator("Offers");
        tabHost.addTab(spec1);
        tabHost.addTab(spec2);
        tabHost.addTab(spec3);

        membersListView = (ListView) tabHost.findViewById(R.id.group_members_list_view);
        membersAdapter = new GroupMembersListViewAdapter(getActivity().getLayoutInflater());
        membersListView.setAdapter(membersAdapter);

        requestsListView = (ListView) tabHost.findViewById(R.id.group_membership_requests_list_view);
        requestsAdapter = new GroupMembershipRequestsListAdapter(getActivity().getLayoutInflater());
        requestsListView.setAdapter(requestsAdapter);
        requestsListView.setOnItemLongClickListener(new MembershipRequestsOnLongClickListener());

        offersListView = (ListView) tabHost.findViewById(R.id.group_membership_offers_list_view);
        offersAdapter = new GroupMembershipOffersListAdapter(getActivity().getLayoutInflater());
        offersListView.setAdapter(offersAdapter);

        loadGroupMembersAndRequests(true);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        requestsActionMode = null;
        requestsActionModeCallback = new MembershipRequestsCallback();

        groupId = getArguments().getLong(GROUP_ID);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (GroupMembersFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MembershipOffersFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefreshStarted(View view) {
        downloadGroupMembersAndRequestsNews();
    }

    @Override
    public void onTabChanged(String tabId) {
        if (tabId.equals(TAB_MEMBERS)) {
            mPullToRefreshLayout = mPullToRefreshMembersLayout;
        } else if (tabId.equals(TAB_REQUESTS)) {
            mPullToRefreshLayout = mPullToRefreshRequestsLayout;
        } else if (tabId.equals(TAB_OFFERS)) {
            mPullToRefreshLayout = mPullToRefreshOffersLayout;
        }
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create()
                        .scrollDistance(.15f)
                        .build())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);
    }

    public void downloadGroupMembersAndRequestsNews() {
        new DownloadGroupMembersAndRequestsNewsTask().execute(groupId);
    }

    private void showAcceptMembershipRequestDialog(Long requestId) {
        DialogFragment dialog = new AcceptMembershipRequestDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(REQUEST_ID, requestId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "confirm_request");
    }

    private void showRejectMembershipRequestDialog(Long requestId) {
        DialogFragment dialog = new RejectMembershipRequestDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(REQUEST_ID, requestId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "reject_request");
    }

    private void loadGroupMembersAndRequests(boolean downloadNews) {
        showProgressBar();
        new LoadGroupMembersAndRequestsFromDatabaseTask().execute(new TaskRequest(groupId, downloadNews));
    }

    private void handleLoadedGroupMembersAndRequests(List<ShareUserDto> members, List<MembershipRequestDto> requests,
                                                     List<MembershipRequestDto> offers, boolean downloadNews) {

        Log.i("SS", "Loading group members and requests from database finished.");
        membersAdapter.addItems(members);
        requestsAdapter.addItems(requests);
        offersAdapter.addItems(offers);

        if (downloadNews) {
            downloadGroupMembersAndRequestsNews();
        } else {
            dismissProgressBar();
        }
    }

    private void showProgressBar() {
        mPullToRefreshLayout.setRefreshing(true);
    }

    private void dismissProgressBar() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    private class LoadGroupMembersAndRequestsFromDatabaseTask extends AsyncTask<TaskRequest, Void, Void> {

        private Long groupId;

        private boolean downloadNews;

        private List<ShareUserDto> members;

        private List<MembershipRequestDto> requests;

        private List<MembershipRequestDto> offers;

        @Override
        protected Void doInBackground(TaskRequest... params) {
            groupId = params[0].getEntityId();
            downloadNews = params[0].isDoSomething();

            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();

            Log.i("SS", "Loading members from database with groupId: " + groupId);
            getMembersOfGroup(db);

            Log.i("SS", "Loading membership requests from database with groupId: " + groupId);
            requests = getMembershipRequestsOfGroupAndType(db, MembershipRequestType.REQUEST);

            Log.i("SS", "Loading membership offers from database with groupId: " + groupId);
            offers = getMembershipRequestsOfGroupAndType(db, MembershipRequestType.OFFER);

            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            handleLoadedGroupMembersAndRequests(members, requests, offers, downloadNews);
        }

        private void getMembersOfGroup(SQLiteDatabase db) {
            members = new ArrayList<ShareUserDto>();
            Cursor cursor = ShoppingShareDatabaseManager.getMembersOfGroup(db, groupId);
            if (cursor == null) {
                return;
            }
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long userId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.UserEntry._ID)
                    );
                    String login = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.UserEntry.COLUMN_NAME_LOGIN)
                    );
                    String firstname = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.UserEntry.COLUMN_NAME_FIRSTNAME)
                    );
                    String lastname = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.UserEntry.COLUMN_NAME_LASTNAME)
                    );
                    members.add(new ShareUserDto(userId, login, firstname, lastname));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
        }

        private List<MembershipRequestDto> getMembershipRequestsOfGroupAndType(SQLiteDatabase db, MembershipRequestType type) {
            List<MembershipRequestDto> membershipRequestDtos = new ArrayList<MembershipRequestDto>();
            Cursor cursor = ShoppingShareDatabaseManager.getMembershipRequestsOfGroupAndType(db, groupId, type);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long requestId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry._ID)
                    );
                    Long userId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_USER_ID)
                    );
                    String login = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LOGIN)
                    );
                    String firstname = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_FIRSTNAME)
                    );
                    String lastname = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LASTNAME)
                    );
                    membershipRequestDtos.add(new MembershipRequestDto(null, requestId, userId, login, firstname, lastname));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return membershipRequestDtos;
        }

    }

    private class DownloadGroupMembersAndRequestsNewsTask extends AsyncTask<Long, Void, Boolean> {

        private Long groupId;

        private Date newSync;

        @Override
        protected Boolean doInBackground(Long... longs) {
            groupId = longs[0];
            Log.i("SS", "Downloading group's members news.");
            Date lastSync = getLastSynchronizationDate();
            Log.i("SS", "Last sync was in: " + lastSync);

            GroupMembersMessage result = downloadGroupMembersNews(lastSync);
            if (result != null) {
                setLastSynchronizationDate(newSync);
                updateDatabaseWithUserNews(result.getUsers());
                updateDatabaseWithNonDealtMembershipRequests(result.getRequests());
                updateDatabaseWithDealtMembershipRequests(result.getDealtRequests());
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean updateGui) {
            if (updateGui) {
                membersAdapter.removeAllItems();
                requestsAdapter.removeAllItems();
                offersAdapter.removeAllItems();
                loadGroupMembersAndRequests(false);
            }
        }

        private Date getLastSynchronizationDate() {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            Cursor cursor = ShoppingShareDatabaseManager.getLastGroupMembersSynchronizationDate(db, groupId);
            cursor.moveToFirst();
            if (cursor.isAfterLast()) {
                Log.i("SS", "Group's members have never been synchronized.");
                ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
                return null;
            } else {
                try {
                    Long time = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE)
                    );
                    return new Date(time);
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                    return null;
                } finally {
                    ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
                }
            }
        }

        private GroupMembersMessage downloadGroupMembersNews(Date lastSync) {
            if (lastSync == null) {
                lastSync = new Date(1);
            }
            NewsRequestMessage message = new NewsRequestMessage(lastSync, groupId);
            try {
                final String url = getString(R.string.base_uri) + "/members";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                // Populate the headers in an HttpEntity object to use for the request
                HttpEntity<NewsRequestMessage> requestEntity = new HttpEntity<NewsRequestMessage>(message, requestHeaders);

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                // Perform the HTTP GET request
                ResponseEntity<GroupMembersMessage> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        GroupMembersMessage.class);

                newSync = new Date(responseEntity.getHeaders().getDate());

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Dowloading group's members news failed." + e.getMessage(), e);
                return null;
            }
        }

        private void setLastSynchronizationDate(Date newSync) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.setLastGroupMembersSynchronizationDate(db, newSync, groupId);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithUserNews(List<ShareUserDto> users) {
            if (users != null) {
                SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
                for (ShareUserDto user: users) {
                    ShoppingShareDatabaseManager.updateOrInsertUser(db, user, groupId);
                }
                ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            }
        }

        private void updateDatabaseWithNonDealtMembershipRequests(List<MembershipRequestDto> dtos) {
            if (dtos != null) {
                SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
                for (MembershipRequestDto dto: dtos) {
                    ShoppingShareDatabaseManager.updateDatabaseWithNonDealtMembershipRequest(db, dto, groupId);
                }
                ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            }
        }

        private void updateDatabaseWithDealtMembershipRequests(List<DealtMembershipRequest> dtos) {
            if (dtos != null) {
                SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
                for (DealtMembershipRequest dto: dtos) {
                    ShoppingShareDatabaseManager.updateDatabaseWithDealtMembershipRequest(db, dto, groupId);
                }
                ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            }
        }

    }

    private class MembershipRequestsOnLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            Long requestId = (Long) view.getTag();
            Log.i("SS", "Long-clicked membership request with id: " + requestId);
            if (requestsActionMode != null) {
                return false;
            }
            requestsActionMode = getActivity().startActionMode(requestsActionModeCallback);
            requestsActionMode.setTag(requestId);
            view.setHovered(true);
            return true;
        }
    }

    private class MembershipRequestsCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_group_members_request_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long requestId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_group_members_accept:
                    Log.i("SS", "Goint to accept request with id: " + requestId);
                    showAcceptMembershipRequestDialog(requestId);
                    requestsActionMode.finish();
                    return true;
                case R.id.menu_fragment_group_members_reject:
                    Log.i("SS", "Goint to reject request with id: " + requestId);
                    showRejectMembershipRequestDialog(requestId);
                    requestsActionMode.finish();
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            requestsListView.setItemChecked(-1, true);
            requestsActionMode = null;
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface GroupMembersFragmentCallbacks {

    }

}
