package com.cvut.shoppingshare.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.StringArrayAdapter;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.TemplateForm;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;


/**
 * Activities that contain this fragment must implement the
 * {@link com.cvut.shoppingshare.fragment.AddTemplateFragment.AddTemplateFragmentCallbacks} interface
 * to handle interaction events.
 * Use the {@link AddTemplateFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class AddTemplateFragment extends Fragment {

    private AddTemplateFragmentCallbacks mListener;

    private EditText templateNameEditText;

    private EditText itemNameEditText;

    private Button addItemButton;

    private Button sendButton;

    private ListView itemsListView;

    private StringArrayAdapter itemsAdapter;

    private ActionMode itemsActionMode;

    private ItemsActionModeCallback itemsActionModeCallback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddTemplateFragment.
     */
    public static AddTemplateFragment newInstance() {
        AddTemplateFragment fragment = new AddTemplateFragment();
        return fragment;
    }
    public AddTemplateFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        itemsActionMode = null;
        itemsActionModeCallback = new ItemsActionModeCallback();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_add_template, container, false);
        templateNameEditText = (EditText) view.findViewById(R.id.fragment_add_template_name);
        itemNameEditText = (EditText) view.findViewById(R.id.fragment_add_template_item_name);

        addItemButton = (Button) view.findViewById(R.id.fragment_add_template_add_item);
        addItemButton.setOnClickListener(new AddItemButtonOnClickListener());

        sendButton = (Button) view.findViewById(R.id.fragment_add_template_create);
        sendButton.setOnClickListener(new SendButtonOnClickListener());

        itemsListView = (ListView) view.findViewById(R.id.fragment_add_template_items_list_view);
        itemsAdapter = new StringArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new ArrayList<String>());
        itemsListView.setAdapter(itemsAdapter);
        itemsListView.setOnItemLongClickListener(new ItemsOnLongClickListener());

        return view;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AddTemplateFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddTemplateFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private TemplateForm getTemplateForm() {
        String name = templateNameEditText.getText().toString();
        if (name.length() < 3) {
            Toast.makeText(getActivity(), "Template name too short! Minimal length is 3.", Toast.LENGTH_SHORT).show();
            return null;
        }
        return new TemplateForm(name, itemsAdapter.getAllItems());
    }

    private void handleResult(boolean success) {
        if (success) {
            Toast.makeText(getActivity(), "Purchase template successfully created!", Toast.LENGTH_SHORT).show();
            mListener.onShowTemplatesSelected();
        } else {
            Toast.makeText(getActivity(), "Sending purchase template failed!", Toast.LENGTH_SHORT).show();
        }
    }

    private class ItemsOnLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            String item = itemsAdapter.getItem(position);
            if (itemsActionMode != null) {
                return false;
            }
            itemsActionMode = getActivity().startActionMode(itemsActionModeCallback);
            itemsActionMode.setTag(item);
            view.setHovered(true);
            return true;
        }

    }

    private class ItemsActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_add_template_items_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            String item = (String) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_add_template_remove_selected_item:
                    Log.i("SS", "Goint to remove item: " + item);
                    itemsAdapter.remove(item);
                    itemsActionMode.finish();
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            itemsListView.setItemChecked(-1, true);
            itemsActionMode = null;
        }

    }

    private class AddItemButtonOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String itemToAdd = itemNameEditText.getText().toString();
            itemsAdapter.add(itemToAdd);
            itemNameEditText.setText("");
        }

    }

    private class SendButtonOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            TemplateForm form = getTemplateForm();
            if (form == null) {
                return;
            }
            new SendFormTask().execute(form);
        }

    }

    private class SendFormTask extends AsyncTask<TemplateForm, Void, Boolean> {

        private TemplateForm form;

        @Override
        protected Boolean doInBackground(TemplateForm... params) {
            form = params[0];
            try {
                Log.i("SS", "Sending TemplateForm with name: " + form.getName());
                final String url = getString(R.string.base_uri) + "/purchasetemplate/form";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<TemplateForm> requestEntity = new HttpEntity<TemplateForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> result =restTemplate.exchange(url, HttpMethod.PUT, requestEntity, ConfirmMessage.class);

                return result.getBody().isOk();
            } catch (Exception e) {
                Log.e("SS", "Uploading template form name " + form.getName() + " resulted in error: " + e + e.getMessage());
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean success) {
            handleResult(success);
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     */
    public interface AddTemplateFragmentCallbacks {

        public void onShowTemplatesSelected();

    }

}
