package com.cvut.shoppingshare.fragment;

import com.cvut.shoppingshare.data.Item;

/**
 * Created by marek on 3/7/14.
 */
public interface ItemAddedListener {

    public void handleItemAdded(Item item, Long ownerId);

}
