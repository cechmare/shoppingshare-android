package com.cvut.shoppingshare.fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.EntityListMessageListAdapter;
import com.cvut.shoppingshare.data.AutocompleteForm;
import com.cvut.shoppingshare.data.EntityListMessage;
import com.cvut.shoppingshare.data.GroupForm;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AddGroupFragment.AddGroupFragmentCallbacks} interface
 * to handle interaction events.
 * Use the {@link AddGroupFragment#newInstance} factory method to
 * create an instance of this fragment.
 *
 */
public class AddGroupFragment extends Fragment implements AdapterView.OnItemClickListener {

    private static final int AUTOCOMPLETE_DELAY = 2000;

    private static final int MESSAGE_TEXT_CHANGED = 0;

    private AddGroupFragmentCallbacks mListener;

    private EditText name;

    private Button createButton;

    private AutoCompleteTextView userAutocomplete;

    private TextWatcher textWatcher;

    private Handler handler;

    private EntityListMessageListAdapter autocompleteAdapter;

    private ListView selectedUsersListView;

    private EntityListMessageListAdapter selectedUsersAdapter;

    private ActionMode selectedUsersActionMode;

    private SelectedUsersActionModeCallback selectedUsersActionModeCallback;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment AddGroupFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddGroupFragment newInstance() {
        AddGroupFragment fragment = new AddGroupFragment();
        return fragment;
    }
    public AddGroupFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        textWatcher = new SearchUserTextWatcher();
        handler = new SearchGroupHandler();
        autocompleteAdapter = new EntityListMessageListAdapter(getActivity(), android.R.layout.simple_list_item_1);
        selectedUsersAdapter = new EntityListMessageListAdapter(getActivity(), android.R.layout.simple_list_item_1);

        selectedUsersActionMode = null;
        selectedUsersActionModeCallback = new SelectedUsersActionModeCallback();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout =  inflater.inflate(R.layout.fragment_add_group, container, false);
        name = (EditText) layout.findViewById(R.id.fragment_add_group_name);
        createButton = (Button) layout.findViewById(R.id.fragment_add_group_create);
        createButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                addGroup();
            }
        });

        userAutocomplete = (AutoCompleteTextView) layout.findViewById(R.id.search_user_autocomplete);
        userAutocomplete.setAdapter(autocompleteAdapter);
        userAutocomplete.addTextChangedListener(textWatcher);
        userAutocomplete.setOnItemClickListener(this);

        selectedUsersListView = (ListView) layout.findViewById(R.id.fragment_add_group_users_list_view);
        selectedUsersListView.setAdapter(selectedUsersAdapter);
        selectedUsersListView.setOnItemLongClickListener(new SelectedUsersOnLongClickListener());

        return layout;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (AddGroupFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement AddGroupFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        selectedUsersAdapter.addEntity(autocompleteAdapter.getEntity(position));
        userAutocomplete.setText("");
    }

    public void addGroup() {
        String selectedName = name.getText().toString();
        if (selectedName.length() < 5) {
            Toast.makeText(getActivity(), getString(R.string.fragment_add_group_name_too_short), Toast.LENGTH_SHORT).show();
            return;
        }
        mListener.showProgressBar();
        GroupForm form = new GroupForm(selectedName);
        form.setUserIdsToInvite(getUserIdsToInvite());
        new AddGroupTask().execute(form);
    }

    private List<Long> getUserIdsToInvite() {
        List<Long> ids= new ArrayList<Long>();
        List<EntityListMessage> selectedUsers = selectedUsersAdapter.getAllEntities();
        for (EntityListMessage user: selectedUsers) {
            ids.add(user.getId());
        }
        return ids;
    }

    private void handleResult(EntityListMessage message) {
        if (message == null) {
            Toast.makeText(getActivity(), "Adding group failed during upload!", Toast.LENGTH_SHORT).show();
            mListener.dismissProgressBar();
        } else {
            Toast.makeText(getActivity(), "Group uploaded successfully!", Toast.LENGTH_SHORT).show();
            Log.i("SS", "Received new group's id: " + message.getId());
            mListener.addCreatedGroup(message);
        }
    }

    private class SearchUserTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            handler.removeMessages(MESSAGE_TEXT_CHANGED);
            handler.sendEmptyMessageDelayed(MESSAGE_TEXT_CHANGED, AUTOCOMPLETE_DELAY);
        }

    }

    private class SearchGroupHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            if (msg.what == MESSAGE_TEXT_CHANGED) {
                String enteredText = userAutocomplete.getText().toString();
                new SearchUserTask().execute(new AutocompleteForm(enteredText));
            }
        }

    }

    private class SearchUserTask extends AsyncTask<AutocompleteForm, Void, EntityListMessage[]> {

        @Override
        protected EntityListMessage[] doInBackground(AutocompleteForm... forms) {
            try {
                final String url = getString(R.string.base_uri) + "/user/startsWith";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<AutocompleteForm> requestEntity = new HttpEntity<AutocompleteForm>(forms[0], requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<EntityListMessage[]> response = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        EntityListMessage[].class);

                return response.getBody();
            } catch (Exception e) {
                Log.e("SS", "Searching for users resulted in: " + e.getMessage(), e);
                return null;
            }
        }

        @Override
        protected void onPostExecute(EntityListMessage[] users) {
            if (users != null) {
                autocompleteAdapter.changeDataset(Arrays.asList(users));
            }
        }

    }

    private class AddGroupTask extends AsyncTask<GroupForm, Void, EntityListMessage> {

        private GroupForm form;

        @Override
        protected EntityListMessage doInBackground(GroupForm... groupForms) {
            form = groupForms[0];
            try {
                Log.i("SS", "Sending GroupForm with name: " + form.getName());
                final String url = getString(R.string.base_uri) + "/group";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<GroupForm> requestEntity = new HttpEntity<GroupForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Long> result =restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Long.class);

                return new EntityListMessage(form.getName(), result.getBody());
            } catch (Exception e) {
                Log.e("SS", "Uploading group with name " + form.getName() + " resulted in error: " + e + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(EntityListMessage message) {
            handleResult(message);
        }

    }

    private class SelectedUsersOnLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            EntityListMessage entity = selectedUsersAdapter.getEntity(position);
            if (selectedUsersActionMode != null) {
                return false;
            }
            selectedUsersActionMode = getActivity().startActionMode(selectedUsersActionModeCallback);
            selectedUsersActionMode.setTag(entity);
            view.setHovered(true);
            return true;
        }

    }

    private class SelectedUsersActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_add_group_selected_users_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            EntityListMessage entity = (EntityListMessage) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_add_group_remove_selected_user:
                    Log.i("SS", "Goint to remove user with name: " + entity.getName());
                    selectedUsersAdapter.removeEntity(entity);
                    selectedUsersActionMode.finish();
                    return true;
                default: return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            selectedUsersListView.setItemChecked(-1, true);
            selectedUsersActionMode = null;
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface AddGroupFragmentCallbacks {

        public void showProgressBar();

        public void dismissProgressBar();

        public void addCreatedGroup(EntityListMessage message);

    }

}
