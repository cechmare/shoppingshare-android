package com.cvut.shoppingshare.fragment;

import com.cvut.shoppingshare.data.ConfirmMessage;

/**
 * Created by marek on 9.3.14.
 */
public interface ItemEditedListener {

    public void handleItemEdited(ConfirmMessage message);

}
