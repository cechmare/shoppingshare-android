package com.cvut.shoppingshare.fragment;


import android.app.Activity;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.GroupPurchasesExpandableListAdapter;
import com.cvut.shoppingshare.dao.LastSynchronizationDao;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.Item;
import com.cvut.shoppingshare.data.ItemDto;
import com.cvut.shoppingshare.data.ItemState;
import com.cvut.shoppingshare.data.NewsRequestMessage;
import com.cvut.shoppingshare.data.Purchase;
import com.cvut.shoppingshare.data.PurchaseDto;
import com.cvut.shoppingshare.data.PurchaseState;
import com.cvut.shoppingshare.data.PurchasesItemsDto;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.dialog.AddItemDialog;
import com.cvut.shoppingshare.dialog.AddPurchaseDialog;
import com.cvut.shoppingshare.dialog.AddTemplateFromExisting;
import com.cvut.shoppingshare.dialog.DeleteItemDialog;
import com.cvut.shoppingshare.dialog.DeletePurchaseDialog;
import com.cvut.shoppingshare.data.PurchaseTemplateSourceEnum;
import com.cvut.shoppingshare.dialog.EditItemDialog;
import com.cvut.shoppingshare.dialog.EditPurchaseOrTemplateDialog;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.Options;
import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;

public class GroupPurchasesFragment extends Fragment implements
        OnRefreshListener, TemplateAddedListener, ItemDeletedListener, ItemAddedListener,
        ItemEditedListener, PurchaseOrTemplateEditedListener {

    private static final String GROUP_ID = "GROUP_ID";

    private static final String PURCHASE_ID = "PURCHASE_ID";

    private static final String ITEM_ID = "ITEM_ID";

    private static final String ENTITY_ID = "ENTITY_ID";

    private static final String TEMPLATE_SOURCE = "TEMPLATE_SOURCE";

    private static final String TEMPLATE = "TEMPLATE";

    private static final String ITEM = "ITEM";

    private static final String ENTITY = "ENTITY";

    private List<Purchase> purchases;

    private GroupPurchasesExpandableListAdapter adapter;

    private PullToRefreshLayout mPullToRefreshLayout;

    private ActionMode itemActionMode;

    private ItemActionModeCallback itemActionModeCallback;

    private ActionMode purchaseActionMode;

    private PurchaseActionModeCallback purchaseActionModeCallback;

    private Long groupId;

    private GroupPurchasesFragmentCallbacks mListener;

    private ExpandableListView purchasesListView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment GroupPurchasesFragment.
     */
    public static GroupPurchasesFragment newInstance(Long groupId) {
        GroupPurchasesFragment fragment = new GroupPurchasesFragment();
        Bundle args = new Bundle();
        args.putLong(GROUP_ID, groupId);
        fragment.setArguments(args);
        return fragment;
    }

    public GroupPurchasesFragment() {
        purchases = new ArrayList<Purchase>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_group_purchases_pullable, container, false);
        mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.fragemnt_group_purchases_pullable);
        ActionBarPullToRefresh.from(getActivity())
                .options(Options.create()
                        .scrollDistance(.15f)
                        .build())
                .allChildrenArePullable()
                .listener(this)
                .setup(mPullToRefreshLayout);

        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        itemActionMode = null;
        itemActionModeCallback = new ItemActionModeCallback();

        purchaseActionMode = null;
        purchaseActionModeCallback = new PurchaseActionModeCallback();

        showProgressBar();

        purchasesListView = (ExpandableListView) getActivity().findViewById(R.id.fragment_group_purchases_expandable_list);
        purchasesListView.setOnItemLongClickListener(new ListViewOnLongClickListener());
        adapter = new GroupPurchasesExpandableListAdapter(purchases, getActivity().getLayoutInflater());
        purchasesListView.setAdapter(adapter);

        groupId = getArguments().getLong(GROUP_ID);
        loadData(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_fragment_group_purchases_members:
                Log.i("SS", "Going to show group members.");
                mListener.showGroupMembersFragment(groupId);
                return true;
            case R.id.menu_fragment_group_purchases_add_purchase:
                showAddPurchaseDialog();
                return true;
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.fragment_group_purchases_menu, menu);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (GroupPurchasesFragmentCallbacks) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement MembershipOffersFragmentCallbacks");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onRefreshStarted(View view) {
        downloadNews();
    }

    @Override
    public void templateAdded(boolean success) {
        if (success) {
            Toast.makeText(getActivity(), "Adding template succeeded!", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(getActivity(), "Adding template failed!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handleItemDeleted(Long entityId) {
        adapter.deleteItemWithId(entityId);
    }

    @Override
    public void handleItemAdded(Item item, Long purchaseId) {
        if (item.getEntityId() == null) {
            Toast.makeText(getActivity(), "Adding item failed!", Toast.LENGTH_SHORT).show();
        } else {
            adapter.addItem(item, purchaseId);
            downloadNews();
        }
    }

    @Override
    public void handleItemEdited(ConfirmMessage message) {
        if (message.isOk()) {
            Toast.makeText(getActivity(), "Item successfully edited!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Item edit failed! Try to refresh.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void handlePurchaseOrTemplateEdited(ConfirmMessage message) {
        if (message.isOk()) {
            Toast.makeText(getActivity(), "Purchase successfully edited!", Toast.LENGTH_SHORT).show();
            downloadNews();
        } else {
            Toast.makeText(getActivity(), "Purchase edit failed! Try to refresh.", Toast.LENGTH_SHORT).show();
        }
    }

    public void addPurchase(Purchase purchase) {
        adapter.addPurchase(purchase);
    }

    public void handlePurchaseDeleted(Long purchaseId) {
        adapter.deletePurchaseWithId(purchaseId);
    }

    public void downloadNews() {
        new DownloadNewsTask().execute();
    }

    private void showProgressBar() {
        mPullToRefreshLayout.setRefreshing(true);
    }

    private void dismissProgressBar() {
        mPullToRefreshLayout.setRefreshComplete();
    }

    private void showAddItemDialog(Long purchaseId) {
        DialogFragment dialog = new AddItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(PURCHASE_ID, purchaseId);
        bundle.putBoolean(TEMPLATE, false);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "add_item");
    }

    private void showAddPurchaseDialog() {
        DialogFragment dialog = new AddPurchaseDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(GROUP_ID, groupId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "add_purchase");
    }

    private void showAddTemplateFromPurchaseDialog(Long purchaseId) {
        DialogFragment dialog = new AddTemplateFromExisting();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(ENTITY_ID, purchaseId);
        bundle.putSerializable(TEMPLATE_SOURCE, PurchaseTemplateSourceEnum.PURCHASE);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "add_template");
    }

    private void showDeleteItemDialog(Long itemId) {
        DialogFragment dialog = new DeleteItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(ITEM_ID, itemId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "delete_item");
    }

    private void showDeletePurchaseDialog(Long purchaseId) {
        DialogFragment dialog = new DeletePurchaseDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putLong(PURCHASE_ID, purchaseId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "delete_purchase");
    }

    private void showEditItemDialog(Long itemId) {
        Item item = adapter.getItemWithId(itemId);
        if (item == null) {
            return;
        }
        DialogFragment dialog = new EditItemDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putSerializable(ITEM, item);
        bundle.putBoolean(TEMPLATE, false);
        bundle.putLong(GROUP_ID, groupId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "edit_item");
    }

    private void showEditPurchaseDialog(Long purchaseId) {
        Purchase purchase = adapter.getPurchaseWithId(purchaseId);
        if (purchase == null) {
            return;
        }
        DialogFragment dialog = new EditPurchaseOrTemplateDialog();
        dialog.setTargetFragment(this, 1);

        Bundle bundle = new Bundle();
        bundle.putSerializable(ENTITY, purchase);
        bundle.putBoolean(TEMPLATE, false);
        bundle.putLong(GROUP_ID, groupId);
        dialog.setArguments(bundle);

        dialog.show(getFragmentManager(), "edit_purchase");
    }

    private void loadData(boolean downloadNews) {
        showProgressBar();
        new LoadDataFromDatabaseTask().execute(downloadNews);
    }

    private void handleLoadedData(List<Purchase> purchases, boolean downloadNews) {
        adapter.addPurchases(purchases);

        if (downloadNews) {
            downloadNews();
        } else {
            dismissProgressBar();
        }
    }

    private class LoadDataFromDatabaseTask extends AsyncTask<Boolean, Void, List<Purchase>> {

        private boolean downloadNews;

        @Override
        protected List<Purchase> doInBackground(Boolean... params) {
            downloadNews = params[0];
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();

            Log.i("SS", "Loading purchases from database.");
            List<Purchase> purchases = getPurchases(db);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return purchases;
        }

        @Override
        protected void onPostExecute(List<Purchase> purchases) {
            handleLoadedData(purchases, downloadNews);
        }

        private List<Purchase> getPurchases(SQLiteDatabase db) {
            List<Purchase> purchases = new ArrayList<Purchase>();
            Cursor cursor = ShoppingShareDatabaseManager.getPurchasesOfGroup(db, groupId);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long purchaseId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseEntry._ID)
                    );
                    String name = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseEntry.COLUMN_NAME_NAME)
                    );
                    List<Item> items = getItemsOfPurchase(db, purchaseId);
                    purchases.add(new Purchase(purchaseId, items, name));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return  purchases;
        }

        private List<Item> getItemsOfPurchase(SQLiteDatabase db, Long purchaseId) {
            List<Item> items = new ArrayList<Item>();
            Cursor cursor = ShoppingShareDatabaseManager.getItemsOfPurchase(db, purchaseId);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long itemId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.ItemEntry._ID)
                    );
                    String name = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.ItemEntry.COLUMN_NAME_NAME)
                    );
                    items.add(new Item(name, itemId));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return items;
        }

    }

    private class DownloadNewsTask extends AsyncTask<Void, Void, Boolean> {

        private Date newSync;

        @Override
        protected Boolean doInBackground(Void... voids) {
            Log.i("SS", "Downloading purchases news.");
            Date lastSync = LastSynchronizationDao.getLastGroupPurchasesDate(groupId, getActivity().getApplicationContext());
            Log.i("SS", "Last sync was in: " + lastSync);

            PurchasesItemsDto result = downloadNews(lastSync);
            if (result != null) {
                setLastSynchronizationDate(newSync);
                updateDatabaseWithPurchasesNews(result.getPurchases());
                updateDatabaseWithItemsNews(result.getItems());
                return true;
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean updateGui) {
            if (updateGui) {
                loadData(false);
            }
        }

        private PurchasesItemsDto downloadNews(Date lastSync) {
            if (lastSync == null) {
                lastSync = new Date(1);
            }
            NewsRequestMessage request = new NewsRequestMessage(lastSync, groupId);
            try {
                final String url = getString(R.string.base_uri) + "/purchase/news";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<NewsRequestMessage> requestEntity = new HttpEntity<NewsRequestMessage>(request, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                ResponseEntity<PurchasesItemsDto> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        PurchasesItemsDto.class);

                newSync = new Date(responseEntity.getHeaders().getDate());

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Dowloading purchases news failed." + e.getMessage(), e);
                return null;
            }
        }

        private void setLastSynchronizationDate(Date newSync) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            ShoppingShareDatabaseManager.setLastGroupPurchasesSynchronizationDate(db, newSync, groupId);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithPurchasesNews(List<PurchaseDto> purchases) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            for (PurchaseDto purchase: purchases) {
                if (purchase.getState() == PurchaseState.DELETED) {
                    ShoppingShareDatabaseManager.removePurchaseWithId(db, purchase.getPurchaseId());
                } else {
                    ShoppingShareDatabaseManager.updatePurchaseWithId(db, purchase, groupId);
                }
            }
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

        private void updateDatabaseWithItemsNews(List<ItemDto> items) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            for (ItemDto item: items) {
                if (item.getState() == ItemState.DELETED) {
                    ShoppingShareDatabaseManager.removeItemWithId(db, item.getItemId());
                } else {
                    ShoppingShareDatabaseManager.updateItemWithId(db, item);
                }
            }
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
        }

    }

    private class ListViewOnLongClickListener implements AdapterView.OnItemLongClickListener {

        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            Long entityId = (Long) view.getTag();
            if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_CHILD) {
                Log.i("SS", "Clicked item with id: " + entityId);
                if (itemActionMode != null) {
                    return false;
                }
                itemActionMode = getActivity().startActionMode(itemActionModeCallback);
                itemActionMode.setTag(entityId);
                view.setHovered(true);
                return true;
            } else if (ExpandableListView.getPackedPositionType(id) == ExpandableListView.PACKED_POSITION_TYPE_GROUP) {
                Log.i("SS", "Clicked group with id: " + entityId);
                if (purchaseActionMode != null) {
                    return false;
                }
                purchaseActionMode = getActivity().startActionMode(purchaseActionModeCallback);
                purchaseActionMode.setTag(entityId);
                view.setHovered(true);
                return true;
            } else {
                return false;
            }
        }
    }

    private class ItemActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_group_purchases_item_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long itemId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_group_purchases_delete_item:
                    Log.i("SS", "Deleting item: " + itemId);
                    showDeleteItemDialog(itemId);
                    return true;
                case R.id.menu_fragment_group_purchases_edit_item:
                    showEditItemDialog(itemId);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            purchasesListView.setItemChecked(-1, true);
            itemActionMode = null;
        }
    }

    private class PurchaseActionModeCallback implements ActionMode.Callback {

        @Override
        public boolean onCreateActionMode(ActionMode actionMode, Menu menu) {
            MenuInflater inflater = actionMode.getMenuInflater();
            inflater.inflate(R.menu.fragment_group_purchases_purchase_menu, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode actionMode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
            Long purchaseId = (Long) actionMode.getTag();
            switch (menuItem.getItemId()) {
                case R.id.menu_fragment_group_purchases_delete_purchase:
                    Log.i("SS", "Deleting purchase with id: " + purchaseId);
                    showDeletePurchaseDialog(purchaseId);
                    return true;
                case R.id.menu_fragment_group_purchases_add_item:
                    Log.i("SS", "Adding item to purchase with id: " + purchaseId);
                    showAddItemDialog(purchaseId);
                    return true;
                case R.id.menu_fragment_group_purchases_template:
                    Log.i("SS", "Creating template from purchase with id: " + purchaseId);
                    showAddTemplateFromPurchaseDialog(purchaseId);
                    return true;
                case R.id.menu_fragment_group_purchases_edit_purchase:
                    Log.i("SS", "Editing purchase with id: " + purchaseId);
                    showEditPurchaseDialog(purchaseId);
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode actionMode) {
            purchasesListView.setItemChecked(-1, true);
            purchaseActionMode = null;
        }
    }

    public interface GroupPurchasesFragmentCallbacks {

        public void showGroupMembersFragment(Long groupId);

    }
}
