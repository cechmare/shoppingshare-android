package com.cvut.shoppingshare.rest;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpEntityEnclosingRequestBase;
import org.apache.http.client.methods.HttpUriRequest;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;

import java.net.URI;

/**
 * Created by marek on 1/15/14.
 */
public class ShoppingShareHttpComponentsClientHttpRequestFactory extends HttpComponentsClientHttpRequestFactory {

    public ShoppingShareHttpComponentsClientHttpRequestFactory() {
        super();
    }

    public ShoppingShareHttpComponentsClientHttpRequestFactory(HttpClient httpClient) {
        super(httpClient);
    }

    @Override
    protected HttpUriRequest createHttpRequest(HttpMethod httpMethod, URI uri) {
        if (HttpMethod.DELETE == httpMethod) {
            return new HttpEntityEnclosingDeleteRequest(uri);
        }
        return super.createHttpRequest(httpMethod, uri);
    }

    private static class HttpEntityEnclosingDeleteRequest extends HttpEntityEnclosingRequestBase {

        private static final String DELETE = "DELETE";

        public HttpEntityEnclosingDeleteRequest(final URI uri) {
            super();
            setURI(uri);
        }

        @Override
        public String getMethod() {
            return DELETE;
        }

    }

}
