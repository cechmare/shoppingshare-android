package com.cvut.shoppingshare.database;

import android.provider.BaseColumns;

import com.cvut.shoppingshare.data.MembershipRequestType;

/**
 * Created by marek on 1/20/14.
 */
public final class ShoppingShareContract {

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String COMMA_SEP = ",";


    public ShoppingShareContract() {
    }

    public static abstract class SharegroupEntry implements BaseColumns {
        public static final String TABLE_NAME = "sharegroup";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_LAST_PURCHASE_UPDATE = "lastupdate";

        public static final String SQL_CREATE = "CREATE TABLE " + SharegroupEntry.TABLE_NAME + " (" +
                SharegroupEntry._ID + " INTEGER PRIMARY KEY," +
                SharegroupEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                SharegroupEntry.COLUMN_NAME_LAST_PURCHASE_UPDATE + TEXT_TYPE +
                " )";

        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + SharegroupEntry.TABLE_NAME;

        public static final String[] GET_ALL_GROUPS_PROJECTION = {
            _ID, COLUMN_NAME_NAME
        };
    }

    public static abstract class PurchaseEntry implements BaseColumns {
        public static final String TABLE_NAME = "purchase";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_NAME_SHAREGROUP_ID = "sharegroup_ID";

        public static final String SQL_CREATE = "CREATE TABLE " + PurchaseEntry.TABLE_NAME + " (" +
                PurchaseEntry._ID + " INTEGER PRIMARY KEY," +
                PurchaseEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                PurchaseEntry.COLUMN_NAME_SHAREGROUP_ID + INTEGER_TYPE + COMMA_SEP +
                "FOREIGN KEY(" + PurchaseEntry.COLUMN_NAME_SHAREGROUP_ID + ")  REFERENCES " +
                    SharegroupEntry.TABLE_NAME + "(_id)" +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + PurchaseEntry.TABLE_NAME;
        public static final String SQL_SELECT_PURCHASES_OF_GROUP = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_NAME_SHAREGROUP_ID + " LIKE ?";
    }

    public static abstract class ItemEntry implements BaseColumns {
        public static final String TABLE_NAME = "item";
        public static final String COLUMN_NAME_NAME = "name";
        public static final String COLUMN_TABLE_PURCHASE_ID = "purchase_ID";
        public static final String COLUMN_TABLE_PURCHASE_TEMPLATE_ID = "purchasetemplate_ID";

        public static final String SQL_CREATE = "CREATE TABLE " + ItemEntry.TABLE_NAME + " (" +
                ItemEntry._ID + " INTEGER PRIMARY KEY," +
                ItemEntry.COLUMN_NAME_NAME + TEXT_TYPE + COMMA_SEP +
                ItemEntry.COLUMN_TABLE_PURCHASE_ID + INTEGER_TYPE + COMMA_SEP +
                ItemEntry.COLUMN_TABLE_PURCHASE_TEMPLATE_ID + INTEGER_TYPE + COMMA_SEP +
                "FOREIGN KEY(" + ItemEntry.COLUMN_TABLE_PURCHASE_ID + ")  REFERENCES " +
                    PurchaseEntry.TABLE_NAME + "(_id)" + COMMA_SEP +
                "FOREIGN KEY(" + ItemEntry.COLUMN_TABLE_PURCHASE_TEMPLATE_ID + ")  REFERENCES " +
                PurchaseTemplateEntry.TABLE_NAME + "(_id)" +
                " )";

        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + ItemEntry.TABLE_NAME;
        public static final String SQL_SELECT_ITEMS_OF_PURCHASE = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TABLE_PURCHASE_ID + " LIKE ?";
        public static final String SQL_SELECT_ITEMS_OF_PURCHASE_TEMPLATE = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_TABLE_PURCHASE_TEMPLATE_ID + " LIKE ?";
    }

    public static abstract class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_LOGIN = "login";
        public static final String COLUMN_NAME_FIRSTNAME = "firstname";
        public static final String COLUMN_NAME_LASTNAME = "lastname";

        public static final String SQL_CREATE = "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                UserEntry._ID + " INTEGER PRIMARY KEY," +
                UserEntry.COLUMN_NAME_LOGIN + TEXT_TYPE + COMMA_SEP +
                UserEntry.COLUMN_NAME_FIRSTNAME + TEXT_TYPE + COMMA_SEP +
                UserEntry.COLUMN_NAME_LASTNAME + TEXT_TYPE +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;
    }

    public static abstract class MembershipRequestEntry implements BaseColumns {
        public static final String TABLE_NAME = "membershiprequest";
        public static final String COLUMN_NAME_TYPE = "type";
        public static final String COLUMN_NAME_LOGIN = "login";
        public static final String COLUMN_NAME_FIRSTNAME = "firstname";
        public static final String COLUMN_NAME_LASTNAME = "lastname";
        public static final String COLUMN_NAME_USER_ID = "user_ID";
        public static final String COLUMN_NAME_SHAREGROUP_ID = "sharegroup_ID";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_TYPE + INTEGER_TYPE + COMMA_SEP +
                COLUMN_NAME_LOGIN + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_FIRSTNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_LASTNAME + TEXT_TYPE + COMMA_SEP +
                COLUMN_NAME_USER_ID + INTEGER_TYPE + COMMA_SEP +
                COLUMN_NAME_SHAREGROUP_ID + INTEGER_TYPE + COMMA_SEP +
                "FOREIGN KEY(" + COLUMN_NAME_SHAREGROUP_ID + ")  REFERENCES " +
                    SharegroupEntry.TABLE_NAME + "(_id)" +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        public static final String SQL_SELECT_MEMBERSHIP_REQUEST_OF_GROUP_AND_TYPE = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + COLUMN_NAME_SHAREGROUP_ID + " LIKE ? AND " +
                COLUMN_NAME_TYPE + " LIKE ?;";
        public static final String SQL_SELECT_MEMBERSHIP_REQUEST_WITH_ID = "SELECT * FROM " + TABLE_NAME +
                " WHERE " + _ID + " LIKE ?;";
    }

    public static abstract class MembershipOfferEntry implements BaseColumns {
        public static final String TABLE_NAME = "membershipoffer";
        public static final String COLUMN_SHAREGROUP_NAME = "sharegroup_name";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_SHAREGROUP_NAME + TEXT_TYPE +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        public static final String SQL_SELECT_ALL = "SELECT * FROM " + TABLE_NAME + ";";
    }

    public static abstract class PurchaseTemplateEntry implements BaseColumns {
        public static final String TABLE_NAME = "purchasetemplate";
        public static final String COLUMN_NAME_NAME = "name";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
                _ID + " INTEGER PRIMARY KEY," +
                COLUMN_NAME_NAME + TEXT_TYPE +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
        public static final String SQL_SELECT_ALL = "SELECT * FROM " + TABLE_NAME + ";";
    }

    public static abstract class LastSynchronization implements BaseColumns {
        public static final String TABLE_NAME = "lastsync";
        public static final String COLUMN_ENTITY_NAME = "name";
        public static final String COLUMN_NAME_LAST_UPDATE = "lastupdate";

        public static final String SQL_CREATE = "CREATE TABLE " + LastSynchronization.TABLE_NAME + " (" +
                LastSynchronization._ID + " INTEGER PRIMARY KEY," +
                LastSynchronization.COLUMN_ENTITY_NAME + TEXT_TYPE + COMMA_SEP +
                LastSynchronization.COLUMN_NAME_LAST_UPDATE + INTEGER_TYPE +
                " )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + LastSynchronization.TABLE_NAME;
        public static final String SQL_SELECT_LAST_UPDATE = "SELECT " + COLUMN_NAME_LAST_UPDATE +
                " FROM LASTSYNC WHERE " + COLUMN_ENTITY_NAME + " LIKE ?;";
        public static final String SQL_SELECT_LAST_GROUP_UPDATE = "SELECT " + COLUMN_NAME_LAST_UPDATE +
                " FROM LASTSYNC WHERE " + COLUMN_ENTITY_NAME + "=\"" + SharegroupEntry.TABLE_NAME + "\";";
        public static final String SQL_SELECT_MEMBERSHIP_OFFERS_UPDATE = "SELECT " + COLUMN_NAME_LAST_UPDATE +
                " FROM LASTSYNC WHERE " + COLUMN_ENTITY_NAME + "=\"" + MembershipOfferEntry.TABLE_NAME + "\";";
        public static final String SQL_SELECT_PURCHASES_TEMPLATES_UPDATE = "SELECT " + COLUMN_NAME_LAST_UPDATE +
                " FROM LASTSYNC WHERE " + COLUMN_ENTITY_NAME + "=\"" + PurchaseTemplateEntry.TABLE_NAME + "\";";
    }

    public static abstract class UserGroupEntry {
        public static final String TABLE_NAME = "user_group";
        public static final String COLUMN_NAME_SHAREGROUP_ID = "sharegroup_ID";
        public static final String COLUMN_NAME_USER_ID = "user_ID";

        public static final String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_NAME_SHAREGROUP_ID + INTEGER_TYPE + COMMA_SEP +
                COLUMN_NAME_USER_ID + INTEGER_TYPE + COMMA_SEP +
                "FOREIGN KEY(" + COLUMN_NAME_SHAREGROUP_ID + ")  REFERENCES " +
                SharegroupEntry.TABLE_NAME + "(_id)" + COMMA_SEP +
                " FOREIGN KEY(" + COLUMN_NAME_USER_ID + ")  REFERENCES " +
                UserEntry.TABLE_NAME + "(_id)" + COMMA_SEP +
                " UNIQUE(" + COLUMN_NAME_SHAREGROUP_ID + ", " + COLUMN_NAME_USER_ID +
                ") ON CONFLICT REPLACE )";
        public static final String SQL_DELETE = "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

}
