package com.cvut.shoppingshare.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.cvut.shoppingshare.data.DealtMembershipRequest;
import com.cvut.shoppingshare.data.EntityListMessage;
import com.cvut.shoppingshare.data.ItemDto;
import com.cvut.shoppingshare.data.MembershipOfferDto;
import com.cvut.shoppingshare.data.MembershipRequestDto;
import com.cvut.shoppingshare.data.MembershipRequestState;
import com.cvut.shoppingshare.data.MembershipRequestType;
import com.cvut.shoppingshare.data.Purchase;
import com.cvut.shoppingshare.data.PurchaseDto;
import com.cvut.shoppingshare.data.ShareUserDto;

import java.util.Date;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by marek on 1/20/14.
 */
public class ShoppingShareDatabaseManager {

    private AtomicInteger mOpenCounter = new AtomicInteger();

    private static ShoppingShareDatabaseManager instance;

    private static SQLiteOpenHelper mDatabaseHelper;

    private SQLiteDatabase mDatabase;

    public static synchronized void initializeInstance(SQLiteOpenHelper helper) {
        if (instance == null) {
            instance = new ShoppingShareDatabaseManager();
            mDatabaseHelper = helper;
        }
    }

    public static synchronized ShoppingShareDatabaseManager getInstance(Context context) {
        if (instance == null) {
            ShoppingShareDatabaseManager.initializeInstance(new ShoppingShareDbHelper(context));
        }

        return instance;
    }

    public SQLiteDatabase openDatabase() {
        if(mOpenCounter.incrementAndGet() == 1) {
            // Opening new database
            mDatabase = mDatabaseHelper.getWritableDatabase();
        }
        return mDatabase;
    }

    public void closeDatabase() {
        if(mOpenCounter.decrementAndGet() == 0) {
            // Closing database
            mDatabase.close();

        }
    }

    private static String makePlaceholders(int len) {
        StringBuilder sb = new StringBuilder(len * 2 - 1);
        sb.append("?");
        for (int i = 1; i < len; i++) {
            sb.append(",?");
        }
        return sb.toString();
    }

    public static Cursor getAllGroups(SQLiteDatabase db) {
        return db.query(
                ShoppingShareContract.SharegroupEntry.TABLE_NAME,
                ShoppingShareContract.SharegroupEntry.GET_ALL_GROUPS_PROJECTION,
                null,
                null,
                null,
                null,
                null);

    }

    public static Cursor getAllMembershipOffers(SQLiteDatabase db) {
        return db.rawQuery(ShoppingShareContract.MembershipOfferEntry.SQL_SELECT_ALL, null);
    }

    public static Cursor getLastGroupSynchronizationDate(SQLiteDatabase db) {
        return db.rawQuery(ShoppingShareContract.LastSynchronization.SQL_SELECT_LAST_GROUP_UPDATE, null);
    }

    public static Cursor getLastGroupMembersSynchronizationDate(SQLiteDatabase db, Long groupId) {
        String[] selectionArgs = { ShoppingShareContract.UserEntry.TABLE_NAME + groupId };
        return db.rawQuery(ShoppingShareContract.LastSynchronization.SQL_SELECT_LAST_UPDATE, selectionArgs);
    }

    public static Cursor getLastMembershipOffersDate(SQLiteDatabase db) {
        return db.rawQuery(ShoppingShareContract.LastSynchronization.SQL_SELECT_MEMBERSHIP_OFFERS_UPDATE, null);
    }

    public static Cursor getLastGroupPurchasesSynchronizationDate(SQLiteDatabase db, Long groupId) {
        String[] selectionArgs = { ShoppingShareContract.PurchaseEntry.TABLE_NAME + groupId };
        return db.rawQuery(ShoppingShareContract.LastSynchronization.SQL_SELECT_LAST_UPDATE, selectionArgs);
    }

    public static Cursor getLastPurchasesTemplatesDate(SQLiteDatabase db) {
        return db.rawQuery(ShoppingShareContract.LastSynchronization.SQL_SELECT_PURCHASES_TEMPLATES_UPDATE, null);
    }

    public static void setLastGroupSynchronizationDate(SQLiteDatabase db, Date newSync) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE, newSync.getTime());

        String selection = ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME + " LIKE ?";
        String[] selectionArgs = { ShoppingShareContract.SharegroupEntry.TABLE_NAME };

        int rowsAffected = db.update(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        Log.i("SS", "Setting last group synchronization date affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME, ShoppingShareContract.SharegroupEntry.TABLE_NAME);
            db.insert(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                    null,
                    values
            );
        }

    }

    public static void setLastMembershipOffersSynchronizationDate(SQLiteDatabase db, Date newSync) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE, newSync.getTime());

        String selection = ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME + " LIKE ?";
        String[] selectionArgs = { ShoppingShareContract.MembershipOfferEntry.TABLE_NAME };

        int rowsAffected = db.update(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        Log.i("SS", "Setting last membership offers synchronization date affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME, ShoppingShareContract.MembershipOfferEntry.TABLE_NAME);
            db.insert(
                    ShoppingShareContract.LastSynchronization.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void setLastGroupMembersSynchronizationDate(SQLiteDatabase db, Date newSync, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE, newSync.getTime());

        String selection = ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME + " LIKE ?";
        String[] selectionArgs = { ShoppingShareContract.UserEntry.TABLE_NAME + groupId };

        int rowsAffected = db.update(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        Log.i("SS", "Setting last group members synchronization date affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME, ShoppingShareContract.UserEntry.TABLE_NAME + groupId);
            db.insert(
                    ShoppingShareContract.LastSynchronization.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void setLastGroupPurchasesSynchronizationDate(SQLiteDatabase db, Date newSync, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE, newSync.getTime());

        String selection = ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME + " LIKE ?";
        String[] selectionArgs = { ShoppingShareContract.PurchaseEntry.TABLE_NAME + groupId };

        int rowsAffected = db.update(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        Log.i("SS", "Setting last group purchases synchronization date affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME, ShoppingShareContract.PurchaseEntry.TABLE_NAME + groupId);
            db.insert(
                    ShoppingShareContract.LastSynchronization.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void setLastPurchasesTemplatesDate(SQLiteDatabase db, Date newSync) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE, newSync.getTime());

        String selection = ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME + " LIKE ?";
        String[] selectionArgs = { ShoppingShareContract.PurchaseTemplateEntry.TABLE_NAME };

        int rowsAffected = db.update(
                ShoppingShareContract.LastSynchronization.TABLE_NAME,
                values,
                selection,
                selectionArgs);
        Log.i("SS", "Setting last purchases templates synchronization date affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.LastSynchronization.COLUMN_ENTITY_NAME, ShoppingShareContract.PurchaseTemplateEntry.TABLE_NAME);
            db.insert(
                    ShoppingShareContract.LastSynchronization.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static boolean updateOrInsertGroup(SQLiteDatabase db, EntityListMessage group) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.SharegroupEntry.COLUMN_NAME_NAME, group.getName());

        String selection = ShoppingShareContract.SharegroupEntry._ID + " LIKE ?";
        String[] selectionArgs = { group.getId().toString() };

        int rowsAffected = db.update(
                ShoppingShareContract.SharegroupEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        Log.i("SS", "UpdateOrInsertGroup affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.SharegroupEntry._ID, group.getId());
            db.insert(
                ShoppingShareContract.SharegroupEntry.TABLE_NAME,
                    null,
                    values
            );
            return false;
        }
        return true;
    }

    public static Cursor getMembersOfGroup(SQLiteDatabase db, Long groupId) {
        String[] selectionArgs = { String.valueOf(groupId) };
        String query1 = "SELECT " + ShoppingShareContract.UserGroupEntry.COLUMN_NAME_USER_ID + " FROM " +
                ShoppingShareContract.UserGroupEntry.TABLE_NAME + " WHERE " + ShoppingShareContract.UserGroupEntry.COLUMN_NAME_SHAREGROUP_ID +
                " LIKE ?;";
        Cursor cursor = db.rawQuery(query1, selectionArgs);

        int idsCount = cursor.getCount();
        if (idsCount == 0) {
            return null;
        }

        String query = "SELECT * FROM " + ShoppingShareContract.UserEntry.TABLE_NAME + " WHERE _id IN (" + makePlaceholders(idsCount) + ")";
        String[] ids = new String[idsCount];

        cursor.moveToFirst();
        int i = 0;

        while(!cursor.isAfterLast()) {
            try {
                ids[i] = cursor.getString(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.UserGroupEntry.COLUMN_NAME_USER_ID)
                );
            } catch (IllegalArgumentException e) {
                Log.e("SS", "Getting database row failed. " + e + e.getMessage());
            }
            cursor.moveToNext();
            i++;
        }

        return db.rawQuery(query, ids);
    }

    public static Cursor getMembershipRequestsOfGroupAndType(SQLiteDatabase db, Long groupId, MembershipRequestType type) {
        String[] selectionArgs = { String.valueOf(groupId), String.valueOf(type.ordinal()) };
        return db.rawQuery(ShoppingShareContract.MembershipRequestEntry.SQL_SELECT_MEMBERSHIP_REQUEST_OF_GROUP_AND_TYPE, selectionArgs);
    }

    public static Cursor getPurchasesOfGroup(SQLiteDatabase db, Long groupId) {
        String[] selectionArgs = { String.valueOf(groupId) };
        return db.rawQuery(ShoppingShareContract.PurchaseEntry.SQL_SELECT_PURCHASES_OF_GROUP, selectionArgs);
    }

    public static Cursor getItemsOfPurchase(SQLiteDatabase db, Long purchaseId) {
        String[] selectionArgs = { String.valueOf(purchaseId) };
        return db.rawQuery(ShoppingShareContract.ItemEntry.SQL_SELECT_ITEMS_OF_PURCHASE, selectionArgs);
    }

    public static Cursor getPurchaseTemplates(SQLiteDatabase db) {
        return db.rawQuery(ShoppingShareContract.PurchaseTemplateEntry.SQL_SELECT_ALL, null);
    }

    public static Cursor getItemsOfPurchaseTemplate(SQLiteDatabase db, Long templateId) {
        String[] selectionArgs = { String.valueOf(templateId) };
        return db.rawQuery(ShoppingShareContract.ItemEntry.SQL_SELECT_ITEMS_OF_PURCHASE_TEMPLATE, selectionArgs);
    }

    public static void updateOrInsertUser(SQLiteDatabase db, ShareUserDto user, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.UserEntry.COLUMN_NAME_LOGIN, user.getLogin());
        values.put(ShoppingShareContract.UserEntry.COLUMN_NAME_FIRSTNAME, user.getFirstname());
        values.put(ShoppingShareContract.UserEntry.COLUMN_NAME_LASTNAME, user.getLastname());

        String selection = ShoppingShareContract.UserEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(user.getUserId()) };

        int rowsAffected = db.update(
                ShoppingShareContract.UserEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        Log.i("SS", "UpdateOrInsertUser affected " + rowsAffected + " rows.");

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.UserEntry._ID, user.getUserId());
            db.insert(
                    ShoppingShareContract.UserEntry.TABLE_NAME,
                    null,
                    values
            );
        }
        insertUserGroup(db, user.getUserId(), groupId);
    }

    public static void updateDatabaseWithNonDealtMembershipRequest(SQLiteDatabase db, MembershipRequestDto dto, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LOGIN, dto.getLogin());
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_FIRSTNAME, dto.getFirstname());
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LASTNAME, dto.getLastname());
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_TYPE, dto.getMembershipRequestType().ordinal());
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_USER_ID, dto.getUserId());
        values.put(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_SHAREGROUP_ID, groupId);
        values.put(ShoppingShareContract.MembershipRequestEntry._ID, dto.getRequestId());
        db.insertWithOnConflict(ShoppingShareContract.MembershipRequestEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static void updateDatabaseWithDealtMembershipRequest(SQLiteDatabase db, DealtMembershipRequest dto, Long groupId) {
        if (dto.getMembershipRequestState() == MembershipRequestState.REJECTED) {
            removeMembershipRequestWithId(db, dto.getRequestId());
            return;
        }

        String[] selectionArgs = { String.valueOf(dto.getRequestId()) };
        Cursor cursor = db.rawQuery(ShoppingShareContract.MembershipRequestEntry.SQL_SELECT_MEMBERSHIP_REQUEST_WITH_ID, selectionArgs);
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            Log.i("SS", "Trying to update membership request with id " + dto.getRequestId() + ", which is unknown.");
        } else {
            try {
                Long userId = cursor.getLong(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_USER_ID)
                );
                String login = cursor.getString(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LOGIN)
                );
                String firstname = cursor.getString(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_FIRSTNAME)
                );
                String lastname = cursor.getString(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.MembershipRequestEntry.COLUMN_NAME_LASTNAME)
                );
                ShareUserDto userToAdd = new ShareUserDto(userId, login, firstname, lastname);
                updateOrInsertUser(db, userToAdd, groupId);
            } catch (IllegalArgumentException e) {
                Log.e("SS", "Getting database row failed. " + e + e.getMessage());
            } finally {
                removeMembershipRequestWithId(db, dto.getRequestId());
            }
        }
    }

    public static void insertMembershipOffer(SQLiteDatabase db, MembershipOfferDto dto) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.MembershipOfferEntry._ID, dto.getRequestId());
        values.put(ShoppingShareContract.MembershipOfferEntry.COLUMN_SHAREGROUP_NAME, dto.getGroupName());

        db.insertWithOnConflict(ShoppingShareContract.MembershipOfferEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    public static void removeItemWithId(SQLiteDatabase db, Long itemId) {
        String selection = ShoppingShareContract.ItemEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(itemId) };
        db.delete(ShoppingShareContract.ItemEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static void removePurchaseWithId(SQLiteDatabase db, Long purchaseId) {
        removeItemsOfPurchaseWithId(db, purchaseId);
        String selection = ShoppingShareContract.PurchaseEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(purchaseId) };
        db.delete(ShoppingShareContract.PurchaseEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static void removePurchaseTemplateWithId(SQLiteDatabase db, Long templateId) {
        removeItemsOfPurchaseTemplateWithId(db, templateId);
        String selection = ShoppingShareContract.PurchaseTemplateEntry._ID+ " LIKE ?";
        String[] selectionArgs = { String.valueOf(templateId) };
        db.delete(ShoppingShareContract.PurchaseTemplateEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static void updateItemWithId(SQLiteDatabase db, ItemDto item) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.ItemEntry.COLUMN_NAME_NAME, item.getName());

        String selection = ShoppingShareContract.ItemEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(item.getItemId()) };

        int rowsAffected = db.update(
                ShoppingShareContract.ItemEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.ItemEntry._ID, item.getItemId());
            values.put(ShoppingShareContract.ItemEntry.COLUMN_TABLE_PURCHASE_ID, item.getPurchaseId());
            db.insert(
                    ShoppingShareContract.ItemEntry.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void updateTemplateItemWithId(SQLiteDatabase db, ItemDto item) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.ItemEntry.COLUMN_NAME_NAME, item.getName());

        String selection = ShoppingShareContract.ItemEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(item.getItemId()) };

        int rowsAffected = db.update(
                ShoppingShareContract.ItemEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );

        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.ItemEntry._ID, item.getItemId());
            values.put(ShoppingShareContract.ItemEntry.COLUMN_TABLE_PURCHASE_TEMPLATE_ID, item.getPurchaseId());
            db.insert(
                    ShoppingShareContract.ItemEntry.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void updatePurchaseWithId(SQLiteDatabase db, PurchaseDto purchase, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.PurchaseEntry.COLUMN_NAME_NAME, purchase.getName());

        String selection = ShoppingShareContract.PurchaseEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(purchase.getPurchaseId()) };

        int rowsAffected = db.update(
                ShoppingShareContract.PurchaseEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.PurchaseEntry._ID, purchase.getPurchaseId());
            values.put(ShoppingShareContract.PurchaseEntry.COLUMN_NAME_SHAREGROUP_ID, groupId);
            db.insert(
                    ShoppingShareContract.PurchaseEntry.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void updateOrInsertPurchaseTemplate(SQLiteDatabase db, PurchaseDto template) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.PurchaseTemplateEntry.COLUMN_NAME_NAME, template.getName());

        String selection = ShoppingShareContract.PurchaseTemplateEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(template.getPurchaseId()) };

        int rowsAffected = db.update(
                ShoppingShareContract.PurchaseTemplateEntry.TABLE_NAME,
                values,
                selection,
                selectionArgs
        );
        if (rowsAffected == 0) {
            values.put(ShoppingShareContract.PurchaseTemplateEntry._ID, template.getPurchaseId());
            db.insert(
                    ShoppingShareContract.PurchaseTemplateEntry.TABLE_NAME,
                    null,
                    values
            );
        }
    }

    public static void removeMembershipOfferWithId(SQLiteDatabase db, Long offerId) {
        String selection = ShoppingShareContract.MembershipOfferEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(offerId) };
        db.delete(ShoppingShareContract.MembershipOfferEntry.TABLE_NAME, selection, selectionArgs);
    }

    private static void insertUserGroup(SQLiteDatabase db, Long userId, Long groupId) {
        ContentValues values = new ContentValues();
        values.put(ShoppingShareContract.UserGroupEntry.COLUMN_NAME_SHAREGROUP_ID, String.valueOf(groupId));
        values.put(ShoppingShareContract.UserGroupEntry.COLUMN_NAME_USER_ID, String.valueOf(userId));

        db.insert(ShoppingShareContract.UserGroupEntry.TABLE_NAME, null, values);
    }

    private static void removeMembershipRequestWithId(SQLiteDatabase db, Long requestId) {
        String selection = ShoppingShareContract.MembershipRequestEntry._ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(requestId) };
        db.delete(ShoppingShareContract.MembershipRequestEntry.TABLE_NAME, selection, selectionArgs);
    }

    private static void removeItemsOfPurchaseWithId(SQLiteDatabase db, Long purchaseId) {
        String selection = ShoppingShareContract.ItemEntry.COLUMN_TABLE_PURCHASE_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(purchaseId) };
        db.delete(ShoppingShareContract.ItemEntry.TABLE_NAME, selection, selectionArgs);
    }

    private static void removeItemsOfPurchaseTemplateWithId(SQLiteDatabase db, Long templateId) {
        String selection = ShoppingShareContract.ItemEntry.COLUMN_TABLE_PURCHASE_TEMPLATE_ID + " LIKE ?";
        String[] selectionArgs = { String.valueOf(templateId) };
        db.delete(ShoppingShareContract.ItemEntry.TABLE_NAME, selection, selectionArgs);
    }

}
