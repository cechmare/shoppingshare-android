package com.cvut.shoppingshare.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;

import java.util.Date;

/**
 * Created by marek on 9.3.14.
 */
public class LastSynchronizationDao {

    public static Date getLastPurchasesTemplatesDate(Context context) {
        SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(context).openDatabase();
        Cursor cursor = ShoppingShareDatabaseManager.getLastPurchasesTemplatesDate(db);
        return dataFromCursor(cursor, context);
    }

    public static Date getLastGroupPurchasesDate(Long groupId, Context context) {
        SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(context).openDatabase();
        Cursor cursor = ShoppingShareDatabaseManager.getLastGroupPurchasesSynchronizationDate(db, groupId);
        return dataFromCursor(cursor, context);
    }

    private static Date dataFromCursor(Cursor cursor, Context context) {
        cursor.moveToFirst();
        if (cursor.isAfterLast()) {
            Log.i("SS", "Never been synchronized.");
            ShoppingShareDatabaseManager.getInstance(context).closeDatabase();
            return null;
        } else {
            try {
                Long time = cursor.getLong(
                        cursor.getColumnIndexOrThrow(ShoppingShareContract.LastSynchronization.COLUMN_NAME_LAST_UPDATE)
                );
                return new Date(time);
            } catch (IllegalArgumentException e) {
                Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                return null;
            } finally {
                ShoppingShareDatabaseManager.getInstance(context).closeDatabase();
            }
        }
    }

}
