package com.cvut.shoppingshare.taskdata;

import com.cvut.shoppingshare.data.Purchase;

/**
 * Created by marek on 10.3.14.
 */
public class EditPurchaseOrTemplateTaskData extends ShoppingShareNetworkTaskData {

    private String name;

    private Purchase entity;

    private boolean template;

    private Long groupId;

    public EditPurchaseOrTemplateTaskData(String url, String cookie, String name, Purchase entity, boolean template, Long groupId) {
        super(url, cookie);
        this.name = name;
        this.entity = entity;
        this.template = template;
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Purchase getEntity() {
        return entity;
    }

    public void setEntity(Purchase entity) {
        this.entity = entity;
    }

    public boolean isTemplate() {
        return template;
    }

    public void setTemplate(boolean template) {
        this.template = template;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

}
