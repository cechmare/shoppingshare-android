package com.cvut.shoppingshare.taskdata;

import com.cvut.shoppingshare.data.Item;

/**
 * Created by marek on 10.3.14.
 */
public class EditItemTaskData extends ShoppingShareNetworkTaskData {

    private String name;

    private Item item;

    private Long groupId;

    private boolean template;

    public EditItemTaskData(String url, String cookie, String name, Item item, Long groupId, boolean template) {
        super(url, cookie);
        this.name = name;
        this.item = item;
        this.groupId = groupId;
        this.template = template;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public boolean isTemplate() {
        return template;
    }

    public void setTemplate(boolean template) {
        this.template = template;
    }

}
