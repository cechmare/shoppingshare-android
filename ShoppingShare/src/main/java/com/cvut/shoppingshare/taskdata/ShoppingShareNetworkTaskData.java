package com.cvut.shoppingshare.taskdata;

/**
 * Created by marek on 10.3.14.
 */
public class ShoppingShareNetworkTaskData {

    private String url;

    private String cookie;

    public ShoppingShareNetworkTaskData(String url, String cookie) {
        this.url = url;
        this.cookie = cookie;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

}
