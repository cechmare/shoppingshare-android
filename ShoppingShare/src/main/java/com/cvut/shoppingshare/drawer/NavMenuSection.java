package com.cvut.shoppingshare.drawer;

/**
 * Created by marek on 1/23/14.
 */
public class NavMenuSection implements NavDrawerItem {

    public static final int SECTION_TYPE = 0;
    private Long id;
    private String label;

    private NavMenuSection() {
    }

    public static NavMenuSection create( Long id, String label ) {
        NavMenuSection section = new NavMenuSection();
        section.setLabel(label);
        return section;
    }

    @Override
    public int getType() {
        return SECTION_TYPE;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public boolean isEnabled() {
        return false;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public boolean updateActionBarTitle() {
        return false;
    }

}
