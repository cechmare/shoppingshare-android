package com.cvut.shoppingshare.drawer;

/**
 * Created by marek on 1/23/14.
 */
public interface NavDrawerItem {

    public Long getId();
    public String getLabel();
    public int getType();
    public boolean isEnabled();
    public boolean updateActionBarTitle();

}
