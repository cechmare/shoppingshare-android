package com.cvut.shoppingshare.task;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.activity.LoginActivity;
import com.cvut.shoppingshare.activity.ShoppingShareMainActivity;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 12/25/13.
 */
public class LogoutTask extends AsyncTask<Void, Void, ConfirmMessage>  {

    private ShoppingShareMainActivity mainActivity;

    public LogoutTask(ShoppingShareMainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    protected ConfirmMessage doInBackground(Void... voids) {
        try {
            // The URL for making the request
            final String url = mainActivity.getString(R.string.base_uri) + "/logout";

            // Obtaining JSESSIONID
            SharedPreferences sharedPref = mainActivity.getSharedPreferences(
                    mainActivity.getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
            String cookie = sharedPref.getString(mainActivity.getString(R.string.preferences_session_id), "def");

            // Set the Accept header for "application/json"
            HttpHeaders requestHeaders = new HttpHeaders();
            requestHeaders.set("Cookie", cookie);
            List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
            acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
            requestHeaders.setAccept(acceptableMediaTypes);

            // Populate the headers in an HttpEntity object to use for the request
            HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);

            // Create a new RestTemplate instance
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
            HttpClient httpClient = HttpUtils.getNewHttpClient();
            restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

            // Perform the HTTP GET request
            ResponseEntity<ConfirmMessage> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
                    ConfirmMessage.class);

            return responseEntity.getBody();
//            return restTemplate.getForObject(url, ConfirmMessage.class);
        } catch (Exception e) {
            Log.e("SS", e.getMessage(), e);
        }
        return null;
    }

    @Override
    protected void onPostExecute(ConfirmMessage message) {
        if (message != null && message.isOk()) {
            Log.d("SS", "Logout successful");
            Toast.makeText(mainActivity, "Logout successful", Toast.LENGTH_SHORT).show();
        } else {
            Log.d("SS", "Logout UNsuccessful");
            Toast.makeText(mainActivity, R.string.menu_main_logout_failed, Toast.LENGTH_SHORT).show();
        }
        Intent intent = new Intent(mainActivity, LoginActivity.class);
        intent.putExtra("TRY_TO_CONNECT", false);
        mainActivity.startActivity(intent);
    }
}
