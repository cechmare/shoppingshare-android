package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.fragment.GroupPurchasesFragment;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by marek on 2/25/14.
 */
public class DeletePurchaseDialog extends DialogFragment {

    private static final String PURCHASE_ID = "PURCHASE_ID";

    private Long purchaseId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        purchaseId = getArguments().getLong(PURCHASE_ID);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete selected purchase?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new DeletePurchaseTask().execute(purchaseId);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        return builder.create();
    }

    private void handlePurchaseDeleted() {
        GroupPurchasesFragment groupPurchasesFragment = (GroupPurchasesFragment) getTargetFragment();
        groupPurchasesFragment.handlePurchaseDeleted(purchaseId);
    }

    private class DeletePurchaseTask extends AsyncTask<Long, Void, ConfirmMessage> {

        private Long purchaseId;

        @Override
        protected ConfirmMessage doInBackground(Long... params) {
            purchaseId = params[0];
            try {
                final String url = getString(R.string.base_uri) + "/purchase";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<Long> requestEntity = new HttpEntity<Long>(purchaseId, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> status = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, ConfirmMessage.class);

                return status.getBody();
            } catch (Exception e) {
                Log.e("SS", "Deleting purchase with id " + purchaseId + " resulted in error: " + e + e.getMessage());
            }
            return new ConfirmMessage(false);
        }

        @Override
        protected void onPostExecute(ConfirmMessage confirmMessage) {
            if (confirmMessage.isOk()) {
                Log.i("SS", "Deleting purchase was successful.");
                handlePurchaseDeleted();
            } else {
                Log.i("SS", "Deleting purchase wasn't successful.");
            }
        }

    }

}
