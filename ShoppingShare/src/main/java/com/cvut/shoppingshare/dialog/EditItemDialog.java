package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.dao.LastSynchronizationDao;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.EditEntityForm;
import com.cvut.shoppingshare.data.Item;
import com.cvut.shoppingshare.fragment.ItemEditedListener;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;
import com.cvut.shoppingshare.taskdata.EditItemTaskData;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

/**
 * Created by marek on 9.3.14.
 */
public class EditItemDialog extends DialogFragment {

    private static final String TEMPLATE = "TEMPLATE";

    private static final String ITEM = "ITEM";

    private static final String GROUP_ID = "GROUP_ID";

    private Long groupId;

    private Item item;

    private boolean template;

    private EditText name;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        item = (Item) getArguments().get(ITEM);
        template = getArguments().getBoolean(TEMPLATE);
        if (!template) {
            groupId = getArguments().getLong(GROUP_ID);
        } else {
            groupId = null;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_add_item, null);
        name = (EditText) dialog.findViewById(R.id.dialog_add_item_name);
        name.setText(item.getName());
        builder.setView(dialog)
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        sendForm();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        builder.setTitle("Edit item");
        return builder.create();
    }

    private void sendForm() {
        String itemName = name.getText().toString();
        if (itemName.length() < 1 || itemName.equals(item.getName())) {
            return;
        } else {
            String url = getString(R.string.base_uri) + "/item";
            SharedPreferences sharedPref = getActivity().getSharedPreferences(
                    getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
            String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

            EditItemTaskData data = new EditItemTaskData(url, cookie, itemName, item, groupId, template);
            new EditItemTask().execute(data);
        }

    }

    private void handleResult(ConfirmMessage message) {
        ItemEditedListener listener = (ItemEditedListener) getTargetFragment();
        listener.handleItemEdited(message);
    }

    private class EditItemTask extends AsyncTask<EditItemTaskData, Void, ConfirmMessage> {

        private EditItemTaskData data;

        @Override
        protected ConfirmMessage doInBackground(EditItemTaskData... params) {
            data = params[0];
            Date lastSync = getLastItemSynchronizationDate();
            if (lastSync == null) {
                return new ConfirmMessage(false);
            }
            EditEntityForm form = new EditEntityForm(lastSync, data.getName(), data.getItem().getEntityId());
            try {
                Log.i("SS", "Sending EditEntityForm with name: " + form.getName() +
                        " and entityId: " + form.getEntityId() + " and last sync: " + form.getLastSync().toString());

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", data.getCookie());
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<EditEntityForm> requestEntity = new HttpEntity<EditEntityForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> result = restTemplate.exchange(data.getUrl(), HttpMethod.POST, requestEntity, ConfirmMessage.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Editing item with name " + form.getName() + " resulted in error: " + e + e.getMessage());
                return new ConfirmMessage(false);
            }
        }

        @Override
        protected void onPostExecute(ConfirmMessage message) {
            handleResult(message);
        }

        private Date getLastItemSynchronizationDate() {
            if (data.isTemplate()) {
                return LastSynchronizationDao.getLastPurchasesTemplatesDate(getActivity().getApplicationContext());
            } else {
                return LastSynchronizationDao.getLastGroupPurchasesDate(data.getGroupId(), getActivity().getApplicationContext());
            }
        }

    }

}
