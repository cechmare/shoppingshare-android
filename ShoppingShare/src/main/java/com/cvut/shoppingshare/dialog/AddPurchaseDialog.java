package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.adapter.StringArrayAdapter;
import com.cvut.shoppingshare.data.DoubleEntityForm;
import com.cvut.shoppingshare.data.EntityForm;
import com.cvut.shoppingshare.data.Purchase;
import com.cvut.shoppingshare.database.ShoppingShareContract;
import com.cvut.shoppingshare.database.ShoppingShareDatabaseManager;
import com.cvut.shoppingshare.fragment.GroupPurchasesFragment;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/25/14.
 */
public class AddPurchaseDialog extends DialogFragment  {

    private static final String GROUP_ID = "GROUP_ID";

    private Long groupId;

    private EditText name;

    private ListView templatesListView;

    private StringArrayAdapter templatesAdapter;

    private List<Purchase> templates;

    private CheckBox useTemplateCheckBox;

    private int templatePosition;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        groupId = getArguments().getLong(GROUP_ID);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_add_purchase, null);
        name = (EditText) dialog.findViewById(R.id.dialog_add_purchase_name);
        builder.setView(dialog)
                .setNeutralButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        sendForm();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        AddPurchaseDialog.this.getDialog().cancel();
                    }
                });
        builder.setTitle("Add purchase");

        useTemplateCheckBox = (CheckBox) dialog.findViewById(R.id.dialog_add_purchase_use_template);
        useTemplateCheckBox.setChecked(false);
        useTemplateCheckBox.setOnCheckedChangeListener(new UseTemplateOnCheckedChangeListener());

        templatePosition = -1;
        templatesListView = (ListView) dialog.findViewById(R.id.dialog_add_purchase_templates_list_view);
        templatesAdapter = new StringArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, new ArrayList<String>());
        templatesListView.setAdapter(templatesAdapter);
        templatesListView.setOnItemClickListener(new TemplateOnItemClickListener());

        new LoadTemplatesTask().execute();
        return builder.create();
    }

    private void sendForm() {
        String purchaseName = name.getText().toString();
        if (purchaseName.length() < 1) {
            return;
        } else {
            processNewPurchase(purchaseName);
        }
    }

    private void processNewPurchase(String purchaseName) {
        if (useTemplateCheckBox.isChecked() && templatePosition != -1) {
            Long templateId = templates.get(templatePosition).getEntityId();
            DoubleEntityForm form = new DoubleEntityForm(purchaseName, templateId, groupId);
            new AddPurchaseFromTemplateTask().execute(form);
        } else {
            EntityForm entityForm = new EntityForm(purchaseName, groupId);
            new AddPurchaseTask().execute(entityForm);
        }
        AddPurchaseDialog.this.getDialog().cancel();
    }

    private void handleResult(EntityForm entityForm, Long purchaseId) {
        if (purchaseId == null) {
            Toast.makeText(getActivity(), "Adding purchase failed!", Toast.LENGTH_SHORT).show();
        } else {
            Purchase purchase = new Purchase(purchaseId, entityForm.getName());
            GroupPurchasesFragment groupPurchasesFragment = (GroupPurchasesFragment) getTargetFragment();
            groupPurchasesFragment.addPurchase(purchase);
        }
    }

    private void handleFromTemplateResult(Long purchaseId) {
        if (purchaseId == null) {
            Toast.makeText(getActivity(), "Adding purchase failed!", Toast.LENGTH_SHORT).show();
        } else {
            GroupPurchasesFragment groupPurchasesFragment = (GroupPurchasesFragment) getTargetFragment();
            groupPurchasesFragment.downloadNews();
        }
    }

    private void templatesLoaded(List<Purchase> templates) {
        this.templates = templates;
        for (Purchase purchase: templates) {
            templatesAdapter.add(purchase.getName());
        }
    }

    private class UseTemplateOnCheckedChangeListener implements CompoundButton.OnCheckedChangeListener {

        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                templatesListView.setVisibility(View.VISIBLE);
            } else {
                templatesListView.setVisibility(View.GONE);
            }
        }

    }

    private class TemplateOnItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
            templatePosition = position;
            templatesListView.clearFocus();
            templatesListView.post(new Runnable() {
                @Override
                public void run() {
                    templatesListView.requestFocusFromTouch();
                    templatesListView.setSelection(position);
                    templatesListView.requestFocus();
                }
            });
        }

    }

    private class LoadTemplatesTask extends AsyncTask<Void, Void, List<Purchase>> {

        @Override
        protected List<Purchase> doInBackground(Void... params) {
            SQLiteDatabase db = ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).openDatabase();
            Log.i("SS", "Loading purchases templates from database.");
            List<Purchase> purchases = getPurchasesTemplates(db);
            ShoppingShareDatabaseManager.getInstance(getActivity().getApplicationContext()).closeDatabase();
            return purchases;
        }

        @Override
        protected void onPostExecute(List<Purchase> templates) {
            templatesLoaded(templates);
        }

        private List<Purchase> getPurchasesTemplates(SQLiteDatabase db) {
            List<Purchase> templates = new ArrayList<Purchase>();
            Cursor cursor = ShoppingShareDatabaseManager.getPurchaseTemplates(db);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                try {
                    Long templateId = cursor.getLong(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseTemplateEntry._ID)
                    );
                    String name = cursor.getString(
                            cursor.getColumnIndexOrThrow(ShoppingShareContract.PurchaseTemplateEntry.COLUMN_NAME_NAME)
                    );
                    templates.add(new Purchase(templateId, name));
                } catch (IllegalArgumentException e) {
                    Log.e("SS", "Getting database row failed. " + e + e.getMessage());
                }
                cursor.moveToNext();
            }
            return  templates;
        }

    }

    private class AddPurchaseTask extends AsyncTask<EntityForm, Void, Long> {

        private EntityForm entityForm;

        @Override
        protected Long doInBackground(EntityForm... params) {
            entityForm = params[0];
            try {
                Log.i("SS", "Sending EntityForm with name: " + entityForm.getName() +
                        " and groupID: " + entityForm.getEntityId());
                final String url = getString(R.string.base_uri) + "/purchase";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<EntityForm> requestEntity = new HttpEntity<EntityForm>(entityForm, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Long> result = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Long.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Uploading purchase with name " + entityForm.getName() + " resulted in error: " + e + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long purchaseId) {
            handleResult(entityForm, purchaseId);
        }

    }

    private class AddPurchaseFromTemplateTask extends AsyncTask<DoubleEntityForm, Void, Long> {

        private DoubleEntityForm form;

        @Override
        protected Long doInBackground(DoubleEntityForm... params) {
            form = params[0];
            try {
                final String url = getString(R.string.base_uri) + "/purchase/fromtemplate";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<DoubleEntityForm> requestEntity = new HttpEntity<DoubleEntityForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Long> result = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Long.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Uploading purchase (from template) with name " + form.getName() + " resulted in error: " + e + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long purchaseID) {
            handleFromTemplateResult(purchaseID);
        }

    }

}
