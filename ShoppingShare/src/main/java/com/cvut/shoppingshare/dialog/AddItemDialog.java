package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.EntityForm;
import com.cvut.shoppingshare.data.Item;
import com.cvut.shoppingshare.fragment.ItemAddedListener;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by marek on 1/15/14.
 */
public class AddItemDialog extends DialogFragment {

    private static final String PURCHASE_ID = "PURCHASE_ID";

    private static final String TEMPLATE = "TEMPLATE";

    private Long purchaseId;

    private EditText name;

    private boolean template;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        purchaseId = getArguments().getLong(PURCHASE_ID);
        template = getArguments().getBoolean(TEMPLATE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_add_item, null);
        name = (EditText) dialog.findViewById(R.id.dialog_add_item_name);
        builder.setView(dialog)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        sendForm();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        AddItemDialog.this.getDialog().cancel();
                    }
                });
        builder.setTitle("Add item");
        return builder.create();

    }

    private void sendForm() {
        String itemName = name.getText().toString();
        if (itemName.length() < 1) {
            return;
        } else {
            EntityForm entityForm = new EntityForm(itemName, purchaseId);
            new AddItemTask().execute(entityForm);
        }
    }

    private void handleResult(EntityForm entityForm, Long itemId) {
        Item item = new Item(entityForm.getName(), itemId);
        ItemAddedListener listener = (ItemAddedListener) getTargetFragment();
        listener.handleItemAdded(item, entityForm.getEntityId());
    }

    private class AddItemTask extends AsyncTask<EntityForm, Void, Long> {

        private EntityForm entityForm;

        @Override
        protected Long doInBackground(EntityForm... params) {
            entityForm = params[0];
            try {
                Log.i("SS", "Sending EntityForm with name: " + entityForm.getName() +
                        " and entityId: " + entityForm.getEntityId());
                String url = getString(R.string.base_uri);
                if (template) {
                    url += "/item/template";
                } else {
                    url += "/item";
                }

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<EntityForm> requestEntity = new HttpEntity<EntityForm>(entityForm, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<Long> result = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, Long.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Uploading item with name " + entityForm.getName() + " resulted in error: " + e + e.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Long itemId) {
            handleResult(entityForm, itemId);
        }
    }

}
