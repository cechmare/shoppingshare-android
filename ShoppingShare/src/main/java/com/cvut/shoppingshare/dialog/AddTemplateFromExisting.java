package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.PurchaseTemplateSourceEnum;
import com.cvut.shoppingshare.data.TemplateFromExistingForm;
import com.cvut.shoppingshare.fragment.MembershipOffersFragment;
import com.cvut.shoppingshare.fragment.TemplateAddedListener;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by marek on 2/28/14.
 */
public class AddTemplateFromExisting extends DialogFragment {

    private static final String TEMPLATE_SOURCE = "TEMPLATE_SOURCE";

    private static final String ENTITY_ID = "ENTITY_ID";

    private Long entityId;

    private PurchaseTemplateSourceEnum templateSource;

    private EditText name;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        entityId = getArguments().getLong(ENTITY_ID);
        templateSource = (PurchaseTemplateSourceEnum) getArguments().getSerializable(TEMPLATE_SOURCE);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_add_template_from_existing, null);
        name = (EditText) dialog.findViewById(R.id.dialog_add_template_from_existing_name);
        builder.setView(dialog)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        sendForm();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        builder.setTitle("Add template");
        return builder.create();
    }

    private void sendForm() {
        String templateName = name.getText().toString();
        if (templateName.length() < 1) {
            return;
        } else {
            TemplateFromExistingForm form = new TemplateFromExistingForm(entityId, templateSource, templateName);
            new AddTemplateFromExistingTask().execute(form);
        }
    }

    private void handleResult(boolean success) {
        TemplateAddedListener listener = (TemplateAddedListener) getTargetFragment();
        listener.templateAdded(success);
    }

    private class AddTemplateFromExistingTask extends AsyncTask<TemplateFromExistingForm, Void, Boolean> {

        private TemplateFromExistingForm form;

        @Override
        protected Boolean doInBackground(TemplateFromExistingForm... params) {
            form = params[0];
            return sendData();
        }

        @Override
        protected void onPostExecute(Boolean success) {
            handleResult(success);
        }

        private boolean sendData() {
            try {
                final String url = getString(R.string.base_uri) + "/purchasetemplate";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<TemplateFromExistingForm> requestEntity = new HttpEntity<TemplateFromExistingForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> result = restTemplate.exchange(url, HttpMethod.PUT, requestEntity, ConfirmMessage.class);

                return result.getBody().isOk();
            } catch (Exception e) {
                Log.e("SS", "Uploading template from existing entity resulted in error: " + e + e.getMessage());
            }
            return false;
        }

    }
}
