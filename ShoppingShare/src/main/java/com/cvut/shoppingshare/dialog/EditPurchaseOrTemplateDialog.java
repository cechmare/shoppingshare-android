package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.dao.LastSynchronizationDao;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.data.EditEntityForm;
import com.cvut.shoppingshare.data.Purchase;
import com.cvut.shoppingshare.fragment.PurchaseOrTemplateEditedListener;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;
import com.cvut.shoppingshare.taskdata.EditPurchaseOrTemplateTaskData;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

/**
 * Created by marek on 10.3.14.
 */
public class EditPurchaseOrTemplateDialog extends DialogFragment {

    private static final String TEMPLATE = "TEMPLATE";

    private static final String ENTITY = "ENTITY";

    private static final String GROUP_ID = "GROUP_ID";

    private Long groupId;

    private boolean template;

    private EditText name;

    private Purchase entity;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        entity = (Purchase) getArguments().getSerializable(ENTITY);
        template = getArguments().getBoolean(TEMPLATE);
        if (!template) {
            groupId = getArguments().getLong(GROUP_ID);
        } else {
            groupId = null;
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialog = inflater.inflate(R.layout.dialog_add_item, null);
        name = (EditText) dialog.findViewById(R.id.dialog_add_item_name);
        name.setText(entity.getName());
        builder.setView(dialog)
                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        sendForm();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dismiss();
                    }
                });
        builder.setTitle("Edit name");
        return builder.create();
    }

    private void sendForm() {
        String entityName = name.getText().toString();
        if (entityName.length() < 1 || entityName.equals(entity.getName())) {
            return;
        } else {
            String url = getString(R.string.base_uri);
            if (template) {
                url += "/purchasetemplate";
            } else {
                url += "/purchase";
            }
            SharedPreferences sharedPref = getActivity().getSharedPreferences(
                    getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
            String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");
            EditPurchaseOrTemplateTaskData data = new EditPurchaseOrTemplateTaskData(url, cookie, entityName, entity, template, groupId);
            new EditPurchaseOrTemplateTask().execute(data);
        }
    }

    private void handleResult(ConfirmMessage message) {
        PurchaseOrTemplateEditedListener listener = (PurchaseOrTemplateEditedListener) getTargetFragment();
        listener.handlePurchaseOrTemplateEdited(message);
    }

    private class EditPurchaseOrTemplateTask extends AsyncTask<EditPurchaseOrTemplateTaskData, Void, ConfirmMessage> {

        private EditPurchaseOrTemplateTaskData data;

        @Override
        protected ConfirmMessage doInBackground(EditPurchaseOrTemplateTaskData... params) {
            data = params[0];
            Date lastSync = getLastSynchronizationDate();
            if (lastSync == null) {
                return new ConfirmMessage(false);
            }
            EditEntityForm form = new EditEntityForm(lastSync, data.getName(), data.getEntity().getEntityId());
            try {
                Log.i("SS", "Sending EditEntityForm with name: " + form.getName() +
                        " and entityId: " + form.getEntityId() + " and last sync: " + form.getLastSync().toString());

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", data.getCookie());
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<EditEntityForm> requestEntity = new HttpEntity<EditEntityForm>(form, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> result = restTemplate.exchange(data.getUrl(), HttpMethod.POST, requestEntity, ConfirmMessage.class);

                return result.getBody();
            } catch (Exception e) {
                Log.e("SS", "Editing purchase or template with name " + form.getName() + " resulted in error: " + e + e.getMessage());
                return new ConfirmMessage(false);
            }
        }

        @Override
        protected void onPostExecute(ConfirmMessage message) {
            handleResult(message);
        }

        private Date getLastSynchronizationDate() {
            if (data.isTemplate()) {
                return LastSynchronizationDao.getLastPurchasesTemplatesDate(getActivity().getApplicationContext());
            } else {
                return LastSynchronizationDao.getLastGroupPurchasesDate(data.getGroupId(), getActivity().getApplicationContext());
            }
        }

    }

}
