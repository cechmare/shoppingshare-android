package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.fragment.MembershipOffersFragment;
import com.cvut.shoppingshare.rest.HttpUtils;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by marek on 2/8/14.
 */
public class AcceptMembershipOfferDialog extends DialogFragment {

    private static final String OFFER_ID = "OFFER_ID";

    private Long offerId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        offerId = getArguments().getLong(OFFER_ID);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Accept selected membership offer?");
        builder.setPositiveButton("Accept", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new AcceptMembershipOfferTask().execute(offerId);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //TODO delete
                Log.i("SS", "Cancelled");
                dismiss();
            }
        });
        return builder.create();
    }

    private void handleResult(ConfirmMessage message) {
        if (message.isOk()) {
            MembershipOffersFragment fragment = (MembershipOffersFragment) getTargetFragment();
            fragment.removeDealtOffer(offerId);
        }
    }

    private class AcceptMembershipOfferTask extends AsyncTask<Long, Void, ConfirmMessage> {

        private Long offerId;

        @Override
        protected ConfirmMessage doInBackground(Long... longs) {
            offerId = longs[0];
            try {
                final String url = getString(R.string.base_uri) + "/membershiprequest/accept";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                // Populate the headers in an HttpEntity object to use for the request
                HttpEntity<Long> requestEntity = new HttpEntity<Long>(offerId, requestHeaders);

                // Create a new RestTemplate instance
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory(httpClient));

                // Perform the HTTP GET request
                ResponseEntity<ConfirmMessage> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity,
                        ConfirmMessage.class);

                return responseEntity.getBody();
            } catch (Exception e) {
                Log.e("SS", "Accepting membership offer with id " + offerId + " resulted in error: " + e + e.getMessage());
            }
            return new ConfirmMessage(false);
        }

        @Override
        protected void onPostExecute(ConfirmMessage message) {
            handleResult(message);
        }
    }

}
