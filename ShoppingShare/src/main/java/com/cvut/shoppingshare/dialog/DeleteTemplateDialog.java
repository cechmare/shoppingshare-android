package com.cvut.shoppingshare.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ConfirmMessage;
import com.cvut.shoppingshare.fragment.TemplatesFragment;
import com.cvut.shoppingshare.rest.HttpUtils;
import com.cvut.shoppingshare.rest.ShoppingShareHttpComponentsClientHttpRequestFactory;

import org.apache.http.client.HttpClient;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * Created by marek on 3/5/14.
 */
public class DeleteTemplateDialog extends DialogFragment {

    private static final String TEMPLATE_ID = "TEMPLATE_ID";

    private Long templateId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        templateId = getArguments().getLong(TEMPLATE_ID);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Delete selected template?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                new DeleteTemplateTask().execute(templateId);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        return builder.create();
    }

    private void handleTemplateDeleted(boolean success) {
        TemplatesFragment fragment = (TemplatesFragment) getTargetFragment();
        fragment.handleTemplateDeleted(success);
    }

    private class DeleteTemplateTask extends AsyncTask<Long, Void, ConfirmMessage> {

        private Long templateId;

        @Override
        protected ConfirmMessage doInBackground(Long... params) {
            templateId = params[0];
            try {
                final String url = getString(R.string.base_uri) + "/purchasetemplate";

                SharedPreferences sharedPref = getActivity().getSharedPreferences(
                        getString(R.string.preferences_file_name), Context.MODE_PRIVATE);
                String cookie = sharedPref.getString(getString(R.string.preferences_session_id), "def");

                HttpHeaders requestHeaders = new HttpHeaders();
                requestHeaders.set("Cookie", cookie);
                requestHeaders.add("Accept", "application/json");
                requestHeaders.add("Content-Type", "application/json");

                HttpEntity<Long> requestEntity = new HttpEntity<Long>(templateId, requestHeaders);

                RestTemplate restTemplate = new RestTemplate();
                HttpClient httpClient = HttpUtils.getNewHttpClient();
                restTemplate.setRequestFactory(new ShoppingShareHttpComponentsClientHttpRequestFactory(httpClient));
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());

                ResponseEntity<ConfirmMessage> status = restTemplate.exchange(url, HttpMethod.DELETE, requestEntity, ConfirmMessage.class);

                return status.getBody();
            } catch (Exception e) {
                Log.e("SS", "Deleting template with id " + templateId + " resulted in error: " + e + e.getMessage());
            }
            return new ConfirmMessage(false);
        }

        @Override
        protected void onPostExecute(ConfirmMessage message) {
            handleTemplateDeleted(message.isOk());
        }

    }
}
