package com.cvut.shoppingshare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.MembershipRequestDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/3/14.
 */
public class GroupMembershipOffersListAdapter extends BaseAdapter {

    List<MembershipRequestDto> offers;

    private LayoutInflater inflater;

    public GroupMembershipOffersListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        offers = new ArrayList<MembershipRequestDto>();
    }

    public GroupMembershipOffersListAdapter(List<MembershipRequestDto> offers, LayoutInflater inflater) {
        this.offers = offers;
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return offers.size();
    }

    @Override
    public Object getItem(int position) {
        return offers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return offers.get(position).getUserId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        MembershipRequestDto request = offers.get(position);
        view = inflater.inflate(R.layout.fragment_group_members_offer_item, null);

        TextView login = (TextView) view.findViewById(R.id.group_offers_list_view_item_login);
        TextView name = (TextView) view.findViewById(R.id.group_offers_list_view_item_name);

        login.setText(request.getLogin());
        name.setText(request.getFirstname() + " " + request.getLastname());

        return view;
    }

    public void addItems(List<MembershipRequestDto> offersToAdd) {
        offers.addAll(offersToAdd);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        offers.clear();
        notifyDataSetChanged();
    }

}
