package com.cvut.shoppingshare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.MembershipRequestDto;
import com.cvut.shoppingshare.data.ShareUserDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/3/14.
 */
public class GroupMembershipRequestsListAdapter extends BaseAdapter {

    private List<MembershipRequestDto> requests;

    private LayoutInflater inflater;

    public GroupMembershipRequestsListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        requests = new ArrayList<MembershipRequestDto>();
    }

    @Override
    public int getCount() {
        return requests.size();
    }

    @Override
    public Object getItem(int position) {
        return requests.get(position);
    }

    @Override
    public long getItemId(int position) {
        return requests.get(position).getUserId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        MembershipRequestDto request = requests.get(position);
        view = inflater.inflate(R.layout.fragment_group_members_request_item, null);
        view.setTag(request.getRequestId());

        TextView login = (TextView) view.findViewById(R.id.group_requests_list_view_item_login);
        TextView name = (TextView) view.findViewById(R.id.group_requests_list_view_item_name);

        login.setText(request.getLogin());
        name.setText(request.getFirstname() + " " + request.getLastname());

        return view;
    }

    public void addItems(List<MembershipRequestDto> requestsToAdd) {
        requests.addAll(requestsToAdd);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        requests.clear();
        notifyDataSetChanged();
    }

}
