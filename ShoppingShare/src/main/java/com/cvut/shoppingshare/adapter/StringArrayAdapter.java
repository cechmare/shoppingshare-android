package com.cvut.shoppingshare.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 3/2/14.
 */
public class StringArrayAdapter extends ArrayAdapter<String> {

    private List<String> items;

    public StringArrayAdapter(Context context, int resource, List<String> items) {
        super(context, resource, items);
        this.items = items;
    }

    public List<String> getAllItems() {
        return items;
    }

}
