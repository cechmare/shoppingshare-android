package com.cvut.shoppingshare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.MembershipOfferDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/7/14.
 */
public class MembershipOffersListAdapter extends BaseAdapter {

    private List<MembershipOfferDto> offers;

    private LayoutInflater inflater;

    public MembershipOffersListAdapter(LayoutInflater inflater) {
        this.inflater = inflater;
        this.offers = new ArrayList<MembershipOfferDto>();
    }

    @Override
    public int getCount() {
        return offers.size();
    }

    @Override
    public Object getItem(int position) {
        return offers.get(position);
    }

    @Override
    public long getItemId(int position) {
        return offers.get(position).getRequestId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        MembershipOfferDto dto = offers.get(position);
        view = inflater.inflate(R.layout.fragment_membership_offers_item, null);
        view.setTag(dto.getRequestId());

        TextView groupName = (TextView) view.findViewById(R.id.user_membership_offer);
        groupName.setText(dto.getGroupName());

        return view;
    }

    public void addItems(List<MembershipOfferDto> offersToAdd) {
        offers.addAll(offersToAdd);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        offers.clear();
        notifyDataSetChanged();
    }

}
