package com.cvut.shoppingshare.adapter;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.cvut.shoppingshare.data.EntityListMessage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/21/14.
 */
public class EntityListMessageListAdapter extends ArrayAdapter<String> {

    private List<EntityListMessage> entities;

    public EntityListMessageListAdapter(Context context, int resource) {
        super(context, resource);
        entities = new ArrayList<EntityListMessage>();
    }

    @Override
    public String getItem(int position) {
        return entities.get(position).getName();
    }

    @Override
    public int getCount() {
        return entities.size();
    }

    public void changeDataset(List<EntityListMessage> newEntities) {
        entities.clear();
        entities.addAll(newEntities);
        notifyDataSetChanged();
    }

    public void addEntity(EntityListMessage entity) {
        entities.add(entity);
        notifyDataSetChanged();
    }

    public void removeEntity(EntityListMessage entity) {
        entities.remove(entity);
        notifyDataSetChanged();
    }

    public EntityListMessage getEntity(int position) {
        return entities.get(position);
    }

    public List<EntityListMessage> getAllEntities() {
        return entities;
    }

}
