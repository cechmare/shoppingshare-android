package com.cvut.shoppingshare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.ShareUserDto;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 2/3/14.
 */
public class GroupMembersListViewAdapter extends BaseAdapter {

    private List<ShareUserDto> members;

    private LayoutInflater inflater;

    public GroupMembersListViewAdapter(LayoutInflater inflater) {
        this.members = new ArrayList<ShareUserDto>();
        this.inflater = inflater;
    }

    @Override
    public int getCount() {
        return members.size();
    }

    @Override
    public Object getItem(int position) {
        return members.get(position);
    }

    @Override
    public long getItemId(int position) {
        return members.get(position).getUserId();
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        ShareUserDto member = members.get(position);
        view = inflater.inflate(R.layout.fragment_group_members_member_item, null);

        TextView login = (TextView) view.findViewById(R.id.group_members_list_view_item_login);
        TextView name = (TextView) view.findViewById(R.id.group_members_list_view_item_name);

        login.setText(member.getLogin());
        name.setText(member.getFirstname() + " " + member.getLastname());

        return view;
    }

    public void addItems(List<ShareUserDto> membersToAdd) {
        members.addAll(membersToAdd);
        notifyDataSetChanged();
    }

    public void removeAllItems() {
        members.clear();
        notifyDataSetChanged();
    }

}
