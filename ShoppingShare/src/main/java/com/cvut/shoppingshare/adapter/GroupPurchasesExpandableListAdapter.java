package com.cvut.shoppingshare.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.cvut.shoppingshare.R;
import com.cvut.shoppingshare.data.Item;
import com.cvut.shoppingshare.data.Purchase;

import java.util.List;

/**
 * Created by marek on 1/8/14.
 */
public class GroupPurchasesExpandableListAdapter extends BaseExpandableListAdapter {

    private List<Purchase> purchases;

    public LayoutInflater inflater;

    public GroupPurchasesExpandableListAdapter(List<Purchase> purchases, LayoutInflater inflater) {
        this.purchases = purchases;
        this.inflater = inflater;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return purchases.get(groupPosition).getItems().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        Item item = (Item) getChild(groupPosition, childPosition);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_group_purchases_child, null);
        }
        TextView text = (TextView) convertView.findViewById(R.id.fragment_group_purchases_child_name);
        text.setText(item.getName());
        convertView.setTag(item.getEntityId());
        convertView.setClickable(false);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return purchases.get(groupPosition).getItems().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return purchases.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return purchases.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.fragment_group_purchases_group, null);
        }
        Purchase purchase = (Purchase) getGroup(groupPosition);
        CheckedTextView checkedTextView = (CheckedTextView) convertView;
        checkedTextView.setText(purchase.getName());
        checkedTextView.setChecked(isExpanded);
        checkedTextView.setTag(purchase.getEntityId());
        checkedTextView.setClickable(false);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public Item getItemWithId(Long itemId) {
        for (Purchase p: purchases) {
            for (Item i: p.getItems()) {
                if (i.getEntityId() == itemId) {
                    return i;
                }
            }
        }
        return null;
    }

    public Purchase getPurchaseWithId(Long purchaseId) {
        for (Purchase p: purchases) {
            if (p.getEntityId() == purchaseId) {
                return p;
            }
        }
        return null;
    }

    public void deleteItemWithId(Long itemId) {
        for (Purchase p: purchases) {
            for (Item i: p.getItems()) {
                if (i.getEntityId() == itemId) {
                    p.removeItem(i);
                    notifyDataSetChanged();
                    return;
                }
            }
        }
    }

    public void deletePurchaseWithId(Long purchaseId) {
        for (Purchase p: purchases) {
            if (p.getEntityId() == purchaseId) {
                purchases.remove(p);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void addItem(Item item, Long purchaseId) {
        for (Purchase p: purchases) {
            if (p.getEntityId() == purchaseId) {
                p.getItems().add(item);
                notifyDataSetChanged();
                return;
            }
        }
    }

    public void addPurchase(Purchase purchase) {
        purchases.add(purchase);
        notifyDataSetChanged();
    }

    public void addPurchases(List<Purchase> purchases) {
        this.purchases.clear();
        this.purchases.addAll(purchases);
        notifyDataSetChanged();
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }
}
