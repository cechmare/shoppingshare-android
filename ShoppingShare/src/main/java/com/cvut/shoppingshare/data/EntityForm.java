package com.cvut.shoppingshare.data;

/**
 * Created by marek on 1/17/14.
 */
public class EntityForm {

    private String name;

    private Long entityId;

    public EntityForm() {
    }

    public EntityForm(String name, Long entityId) {
        this.name = name;
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}
