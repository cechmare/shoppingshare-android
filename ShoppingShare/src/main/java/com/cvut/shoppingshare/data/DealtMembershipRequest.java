package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 2/1/14.
 */
public class DealtMembershipRequest {

    private Long requestId;

    private MembershipRequestState membershipRequestState;

    @JsonCreator
    public DealtMembershipRequest(@JsonProperty("requestId") Long requestId, @JsonProperty("membershipRequestState") MembershipRequestState membershipRequestState) {
        this.requestId = requestId;
        this.membershipRequestState = membershipRequestState;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public MembershipRequestState getMembershipRequestState() {
        return membershipRequestState;
    }

    public void setMembershipRequestState(MembershipRequestState membershipRequestState) {
        this.membershipRequestState = membershipRequestState;
    }

}
