package com.cvut.shoppingshare.data;

import java.util.List;

/**
 * Created by marek on 3/2/14.
 */
public class TemplateForm {

    private String name;

    private List<String> items;

    public TemplateForm(String name, List<String> items) {
        this.name = name;
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

}
