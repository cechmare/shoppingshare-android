package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by marek on 2/24/14.
 */
public class PurchasesItemsDto {

    private List<PurchaseDto> purchases;

    private List<ItemDto> items;

    @JsonCreator
    public PurchasesItemsDto(@JsonProperty("purchases") List<PurchaseDto> purchases, @JsonProperty("items") List<ItemDto> items) {
        this.purchases = purchases;
        this.items = items;
    }

    public List<PurchaseDto> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<PurchaseDto> purchases) {
        this.purchases = purchases;
    }

    public List<ItemDto> getItems() {
        return items;
    }

    public void setItems(List<ItemDto> items) {
        this.items = items;
    }

}
