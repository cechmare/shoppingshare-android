package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 2/24/14.
 */
public class PurchaseDto {

    private Long purchaseId;

    private String name;

    private PurchaseState state;

    @JsonCreator
    public PurchaseDto(@JsonProperty("purchaseId") Long purchaseId, @JsonProperty("name") String name, @JsonProperty("state") PurchaseState state) {
        this.purchaseId = purchaseId;
        this.name = name;
        this.state = state;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PurchaseState getState() {
        return state;
    }

    public void setState(PurchaseState state) {
        this.state = state;
    }

}
