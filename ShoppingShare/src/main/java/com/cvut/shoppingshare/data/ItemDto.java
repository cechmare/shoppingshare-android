package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 2/24/14.
 */
public class ItemDto {

    private Long purchaseId;

    private Long itemId;

    private String name;

    private ItemState state;

    @JsonCreator
    public ItemDto(@JsonProperty("purchaseId") Long purchaseId, @JsonProperty("itemId") Long itemId,
                   @JsonProperty("name") String name, @JsonProperty("state") ItemState state) {
        this.purchaseId = purchaseId;
        this.itemId = itemId;
        this.name = name;
        this.state = state;
    }

    public Long getPurchaseId() {
        return purchaseId;
    }

    public void setPurchaseId(Long purchaseId) {
        this.purchaseId = purchaseId;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ItemState getState() {
        return state;
    }

    public void setState(ItemState state) {
        this.state = state;
    }

}
