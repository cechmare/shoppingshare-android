package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/4/14.
 */
public class TaskRequest {

    private Long entityId;

    private boolean doSomething;

    public TaskRequest(Long entityId, boolean doSomething) {
        this.entityId = entityId;
        this.doSomething = doSomething;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public boolean isDoSomething() {
        return doSomething;
    }

    public void setDoSomething(boolean doSomething) {
        this.doSomething = doSomething;
    }
}
