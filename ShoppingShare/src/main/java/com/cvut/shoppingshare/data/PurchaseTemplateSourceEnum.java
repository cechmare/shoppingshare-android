package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/28/14.
 */
public enum PurchaseTemplateSourceEnum {
    PURCHASE, TEMPLATE
}
