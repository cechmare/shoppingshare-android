package com.cvut.shoppingshare.data;

/**
 * Created by marek on 16.4.14.
 */
public class DoubleEntityForm extends EntityForm {

    private Long entityId2;

    public DoubleEntityForm(String name, Long entityId, Long entityId2) {
        super(name, entityId);
        this.entityId2 = entityId2;
    }

    public Long getEntityId2() {
        return entityId2;
    }

    public void setEntityId2(Long entityId2) {
        this.entityId2 = entityId2;
    }

}
