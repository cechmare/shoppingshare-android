package com.cvut.shoppingshare.data;

import java.io.Serializable;

/**
 * Created by marek on 1/7/14.
 */
public class Item implements Serializable {

    private String name;

    private Long entityId;

    public Item() {
    }

    public Item(String name, Long entityId) {
        this.name = name;
        this.entityId = entityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }
}
