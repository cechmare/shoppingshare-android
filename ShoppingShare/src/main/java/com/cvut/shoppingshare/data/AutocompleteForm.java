package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/21/14.
 */
public class AutocompleteForm {

    private String tag;

    public AutocompleteForm(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
