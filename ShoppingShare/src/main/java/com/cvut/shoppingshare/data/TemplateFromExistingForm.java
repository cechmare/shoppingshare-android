package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/28/14.
 */
public class TemplateFromExistingForm {

    private Long entityId;

    private PurchaseTemplateSourceEnum templateSource;

    private String name;

    public TemplateFromExistingForm(Long entityId, PurchaseTemplateSourceEnum templateSource, String name) {
        this.entityId = entityId;
        this.templateSource = templateSource;
        this.name = name;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public PurchaseTemplateSourceEnum getTemplateSource() {
        return templateSource;
    }

    public void setTemplateSource(PurchaseTemplateSourceEnum templateSource) {
        this.templateSource = templateSource;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
