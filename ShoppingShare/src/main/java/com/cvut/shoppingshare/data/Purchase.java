package com.cvut.shoppingshare.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 1/7/14.
 */
public class Purchase implements Serializable {

    private Long entityId;

    private List<Item> items;

    private String name;

    public Purchase() {
    }

    public Purchase(Long entityId, String name) {
        this.entityId = entityId;
        this.name = name;
        this.items = new ArrayList<Item>();
    }

    public Purchase(Long entityId, List<Item> items, String name) {
        this.entityId = entityId;
        this.items = items;
        this.name = name;
    }

    public void removeItem(Item item) {
        items.remove(item);
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
