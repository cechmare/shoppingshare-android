package com.cvut.shoppingshare.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by marek on 1/28/14.
 */
public class GroupForm {

    private String name;

    private List<Long> userIdsToInvite;

    public GroupForm() {
        userIdsToInvite = new ArrayList<Long>();
    }

    public GroupForm(String name) {
        this();
        this.name = name;
    }

    public GroupForm(String name, List<Long> userIdsToInvite) {
        this.name = name;
        this.userIdsToInvite = userIdsToInvite;
    }

    public void addUserToInvite(Long userId) {
        userIdsToInvite.add(userId);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Long> getUserIdsToInvite() {
        return userIdsToInvite;
    }

    public void setUserIdsToInvite(List<Long> userIdsToInvite) {
        this.userIdsToInvite = userIdsToInvite;
    }
}
