package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by marek on 2/1/14.
 */
public class GroupMembersMessage {

    private List<ShareUserDto> users;

    private List<MembershipRequestDto> requests;

    private List<DealtMembershipRequest> dealtRequests;

    @JsonCreator
    public GroupMembersMessage(@JsonProperty("users") List<ShareUserDto> users, @JsonProperty("requests") List<MembershipRequestDto> requests, @JsonProperty("dealtRequests") List<DealtMembershipRequest> dealtRequests) {
        this.users = users;
        this.requests = requests;
        this.dealtRequests = dealtRequests;
    }

    public List<ShareUserDto> getUsers() {
        return users;
    }

    public void setUsers(List<ShareUserDto> users) {
        this.users = users;
    }

    public List<MembershipRequestDto> getRequests() {
        return requests;
    }

    public void setRequests(List<MembershipRequestDto> requests) {
        this.requests = requests;
    }

    public List<DealtMembershipRequest> getDealtRequests() {
        return dealtRequests;
    }

    public void setDealtRequests(List<DealtMembershipRequest> dealtRequests) {
        this.dealtRequests = dealtRequests;
    }

}
