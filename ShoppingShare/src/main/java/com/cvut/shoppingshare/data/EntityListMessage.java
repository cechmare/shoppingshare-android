package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 12/15/13.
 */
public class EntityListMessage {

    private String name;

    private Long id;

    @JsonCreator
    public EntityListMessage(@JsonProperty("name") String name, @JsonProperty("id") Long id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return name;
    }
}
