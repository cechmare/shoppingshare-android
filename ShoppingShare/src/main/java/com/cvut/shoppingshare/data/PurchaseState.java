package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/24/14.
 */
public enum PurchaseState {
    NEW, DELETED
}
