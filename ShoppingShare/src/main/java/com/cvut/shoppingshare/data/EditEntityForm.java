package com.cvut.shoppingshare.data;

import java.util.Date;

/**
 * Created by marek on 9.3.14.
 */
public class EditEntityForm {

    private Date lastSync;

    private String name;

    private Long entityId;

    public EditEntityForm(Date lastSync, String name, Long entityId) {
        this.lastSync = lastSync;
        this.name = name;
        this.entityId = entityId;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}
