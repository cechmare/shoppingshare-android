package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 2/7/14.
 */
public class MembershipOfferDto {

    private Long requestId;

    private String groupName;

    @JsonCreator
    public MembershipOfferDto(@JsonProperty("requestId") Long requestId, @JsonProperty("groupName") String groupName) {
        this.requestId = requestId;
        this.groupName = groupName;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

}
