package com.cvut.shoppingshare.data;

/**
 * Created by marek on 2/1/14.
 */
public enum MembershipRequestType {
    OFFER, REQUEST
}
