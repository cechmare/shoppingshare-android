package com.cvut.shoppingshare.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by marek on 2/1/14.
 */
public class ShareUserDto {

    private Long userId;

    private String login;

    private String firstname;

    private String lastname;

    @JsonCreator
    public ShareUserDto(@JsonProperty("userId") Long userId, @JsonProperty("login") String login, @JsonProperty("firstname") String firstname, @JsonProperty("lastname") String lastname) {
        this.userId = userId;
        this.lastname = lastname;
        this.firstname = firstname;
        this.login = login;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

}
