package com.cvut.shoppingshare.data;

import java.util.Date;

/**
 * Created by marek on 2/1/14.
 */
public class NewsRequestMessage {

    private Date lastSync;

    private Long entityId;

    public NewsRequestMessage(Date lastSync, Long entityId) {
        this.lastSync = lastSync;
        this.entityId = entityId;
    }

    public Date getLastSync() {
        return lastSync;
    }

    public void setLastSync(Date lastSync) {
        this.lastSync = lastSync;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

}
